name := "symexecEVM"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
  "com.beachape" %% "enumeratum" % "1.5.12",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "com.github.scopt" %% "scopt" % "3.7.0",
  "com.typesafe.play" %% "play-json" % "2.6.7"
)

enablePlugins(Antlr4Plugin)

antlr4GenListener in Antlr4 := true // default: true

antlr4GenVisitor in Antlr4 := true // default: false

mainClass in (Compile,run) := Some("Main")