import frontend.ast.InstructionList
import frontend.cfg.BasicBlockBuilder
import frontend.config.{INPUT_TYPE, MyConfig}
import frontend.parser.{Input, Parser}
import backend.symexec.{FullSymbolicExecution, ProgramState}
import com.typesafe.scalalogging.Logger

import scala.collection.mutable.ArrayBuffer

object Main {
  val logger = Logger("Main")


  def runAnalysis(inputs: ArrayBuffer[Input], parser: Parser) = {
    for (inp <- inputs){
      logger.info("Analyzing contract:" + inp.contract)
      // Parser the asm file and also build a source map to map from
      // instruction to its position in source code. The parameter inp.sourceMap
      // is a mutable object, which will be modified during parsing to build the source map

      val prog = parser.parseEvmDisasm(inp.disasmFile, inp.sourceMap)
      val basicBlocksGraph = BasicBlockBuilder.buildBasicBlocks(prog.asInstanceOf[InstructionList])
      // For testing
      //println("XXXXXX " +inp.sourceMap.getSourceCode(133))
      //println(inp.sourceMap.getAST(133))
      //println(inp.sourceMap.getVarName(133))
      // End testing
      val symExec = new FullSymbolicExecution(inp) // need input.disasm_file input.source_map as well
      basicBlocksGraph.accept(symExec, new ProgramState)
    }
  }

  def analyze() = {
    val parser = new Parser()
    val inputs = parser.getInputs()
    runAnalysis(inputs, parser)
  }

  def main(args: Array[String]): Unit = {

    MyConfig.args = args

    // D:\workspace\symexecEVM\src\main\scala\testFastParse\test.evm
    if(MyConfig.config == null) return
    logger.debug(MyConfig.config.toString)

    analyze()
  }
}
