package backend.solver.converter

/*import common.coreast._
import z3.scala.Z3Context

/**
  * Created by dxble on 7/24/16.
  */
class ToZ3ASTConverter(z3: Z3Context) extends CASTVisitorBase{
  override def visitFunctionSignature(functionDesign: FunctionSignature): Z3ASTReturnType ={
    null
  }
  override def visitGreater(greater: Greater): Z3ASTReturnType = {
    val left = greater.opr1.accept(this)
    val right = greater.opr2.accept(this)
    val ast = z3.mkGT(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitLessThan(lessThan: LessThan): Z3ASTReturnType ={
    val left = lessThan.opr1.accept(this)
    val right = lessThan.opr2.accept(this)
    val ast = z3.mkLT(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitLTE(lTE: LTE): Z3ASTReturnType ={
    val left = lTE.opr1.accept(this)
    val right = lTE.opr2.accept(this)
    val ast = z3.mkLE(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitGTE(gTE: GTE): Z3ASTReturnType ={
    val left = gTE.opr1.accept(this)
    val right = gTE.opr2.accept(this)
    val ast = z3.mkGE(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitEqual(equal: Equal): Z3ASTReturnType={
    val left = equal.opr1.accept(this)
    val right = equal.opr2.accept(this)
    val ast = z3.mkEq(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitAnd(and: And): Z3ASTReturnType={
    val left = and.opr1.accept(this)
    val right = and.opr2.accept(this)
    val ast = z3.mkAnd(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitOr(or: Or): Z3ASTReturnType={
    val left = or.opr1.accept(this)
    val right = or.opr2.accept(this)
    val ast = z3.mkOr(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitNot(not: Not): Z3ASTReturnType={
    val op = not.opr.accept(this)
    val ast = z3.mkNot(op.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitImplies(implies: Implies): Z3ASTReturnType={
    val left = implies.opr1.accept(this)
    val right = implies.opr2.accept(this)
    val ast = z3.mkImplies(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitBoolVar(boolVar: BoolVar): Z3ASTReturnType ={
    val bvar = z3.mkBoolConst(z3.mkStringSymbol(boolVar.name))
    new Z3ASTReturnType(bvar)
  }

  override def visitBoolConst(boolConst: BoolConst): Z3ASTReturnType={
    val bconst = if(boolConst.value.toBoolean) z3.mkTrue() else z3.mkFalse()
    new Z3ASTReturnType(bconst)
  }

  override def visitPlus(plus: Plus): Z3ASTReturnType={
    val left = plus.opr1.accept(this)
    val right = plus.opr2.accept(this)
    val ast = z3.mkAdd(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitMinus(minus: Minus): Z3ASTReturnType={
    val left = minus.opr1.accept(this)
    val right = minus.opr2.accept(this)
    val ast = z3.mkSub(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitDiv(div: Div): Z3ASTReturnType={
    val left = div.opr1.accept(this)
    val right = div.opr2.accept(this)
    val ast = z3.mkDiv(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitMult(mult: Mult): Z3ASTReturnType={
    val left = mult.opr1.accept(this)
    val right = mult.opr2.accept(this)
    val ast = z3.mkMul(left.asInstanceOf[Z3ASTReturnType].getAST(), right.asInstanceOf[Z3ASTReturnType].getAST())
    new Z3ASTReturnType(ast)
  }

  override def visitIntConst(intConst: IntConst): Z3ASTReturnType={
    val isort = z3.mkIntSort()
    val iconst = z3.mkInt(intConst.value.toInt, isort)
    new Z3ASTReturnType(iconst)
  }

  override def visitIntVar(intVar: IntVar): Z3ASTReturnType = {
    val ivar = z3.mkIntConst(z3.mkStringSymbol(intVar.name))
    new Z3ASTReturnType(ivar)
  }
}
*/