package backend.solver.coreast

import backend.solver.coreast.CASTNodeKind.CASTNodeKind

/**
  * Created by dxble on 6/21/16.
  */

object CASTNodeKind extends Enumeration {
  type CASTNodeKind = Value
  val BOOL_KIND,
  ARITHMETIC_KIND = Value
}

abstract class CAST extends VisitorReturnType {

  protected def doAccept[T <: CAST](visitorBase: CASTVisitorBase, visit: T => VisitorReturnType, v: T): VisitorReturnType ={
    val (prevRes, toVisit) = visitorBase.preVisit(v)
    if(toVisit) {
      val res = visit(v)
      visitorBase.postVisit(v)
      res
    }else prevRes
  }

  def accept(visitorBase: CASTVisitorBase): VisitorReturnType
  def topLevelString(): String
  def smtString(): String
  def mangledString(): String = toString
  def size(): Int
  override def eq(that: VisitorReturnType): Boolean = this.equals(that)

  def getSort(): CASTNodeKind = {
    if(this.isInstanceOf[BoolOperatorsCAST])
      return CASTNodeKind.BOOL_KIND
    else if(this.isInstanceOf[ArithOperatorsCAST])
      return CASTNodeKind.ARITHMETIC_KIND
    else
      throw new RuntimeException("Not yet designed operator type!")
  }

  def myClone(): CAST
}
sealed trait InternalTerm extends CAST
sealed trait VarTerm extends InternalTerm{
  def getName(): String
}
sealed trait ConstTerm extends InternalTerm

trait BinOp extends CAST{
  def getLeft(): CAST
  def getRight(): CAST
}
trait UnOp extends CAST{
  def getOperand(): CAST
}
abstract class BoolOperatorsCAST extends CAST{
  override def getSort(): CASTNodeKind = CASTNodeKind.BOOL_KIND
}
case class Greater(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+ " > "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " > "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase,visitorBase.visitGreater, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = ">"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(> "+opr1.smtString()+" "+opr2.smtString+")"

  override def myClone(): CAST = new Greater(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])
}
case class LessThan(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+ " < "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " < "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitLessThan, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "<"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(< "+opr1.smtString+ " "+opr2.smtString+")"

  override def myClone(): CAST = new LessThan(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])
}
case class LTE(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+ " <= "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " <= "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitLTE, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "<="

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(<= "+opr1.smtString+ " "+opr2.smtString+")"

  override def myClone(): CAST = new LTE(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])

}

case class GTE(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends BoolOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+ " >= "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+ " >= "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitGTE, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = ">="

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(>= "+opr1.smtString+ " "+opr2.smtString+")"

  override def myClone(): CAST = new GTE(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])

}
case class Equal(opr1: CAST, opr2: CAST) extends BoolOperatorsCAST with BinOp {

  override def toString() : String ={
    "("+opr1.toString+" == "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" == "+opr2.mangledString+")"
  }

  @throws(classOf[DivisionByZeroException])
  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitEqual, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "="

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String =  "(= "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Equal(opr1.myClone(), opr2.myClone())

}
case class Not(opr: BoolOperatorsCAST) extends BoolOperatorsCAST with UnOp{

  override def toString() : String ={
    "!("+opr.toString+")"
  }

  override def mangledString() : String ={
    // Normalize the not (a == b) to a != b
    // This avoid unnecessary actions when using GumTree to measure tree differences
    if(opr.isInstanceOf[Equal]){
      val opr1 = opr.asInstanceOf[Equal].opr1
      val opr2 = opr.asInstanceOf[Equal].opr2
      "("+opr1.mangledString+" != "+opr2.mangledString+")"
    }else {
      "!(" + opr.mangledString + ")"
    }
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitNot, this)
  }

  override def size(): Int = opr.size() + 1

  override def topLevelString(): String = "not"

  override def getOperand(): CAST = opr

  override def smtString(): String =  "(not "+opr.smtString()+")"

  override def myClone(): CAST = new Not(opr.myClone().asInstanceOf[BoolOperatorsCAST])

}
case class And(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST) extends BoolOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+" && "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" && "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitAnd, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "and"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(and "+opr1.smtString()+" "+opr2.smtString()+")"

  override def myClone(): CAST = new And(opr1.myClone().asInstanceOf[BoolOperatorsCAST], opr2.myClone().asInstanceOf[BoolOperatorsCAST])

}
case class Or(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST) extends BoolOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+" || "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" || "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitOr, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "or"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(or "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Or(opr1.myClone().asInstanceOf[BoolOperatorsCAST], opr2.myClone().asInstanceOf[BoolOperatorsCAST])

}
case class Implies(opr1: BoolOperatorsCAST, opr2: BoolOperatorsCAST) extends BoolOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+" => "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" => "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitImplies, this)
  }

  override def size(): Int = opr1.size() + opr2.size() + 1

  override def topLevelString(): String = "=>"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(=> "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Implies(opr1.myClone().asInstanceOf[BoolOperatorsCAST], opr2.myClone().asInstanceOf[BoolOperatorsCAST])

}
case class BoolVar(name: String) extends BoolOperatorsCAST with VarTerm{

  override def getName(): String = name

  override def toString() : String ={
    name
  }

  override def mangledString() : String ={
    name
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitBoolVar, this)
    //new BoolType(this, opr1.accept(visitorBase).asInstanceOf[BoolType].or(opr2.accept(visitorBase).asInstanceOf[BoolType]))
  }
  override def size(): Int = 1

  override def topLevelString(): String = name

  override def smtString(): String = toString()

  override def myClone(): CAST = new BoolVar(name)

  override def hashCode(): Int = name.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[BoolVar])
      return false
    else{
      that.asInstanceOf[BoolVar].name.compareTo(name) == 0
    }
  }
}
case class BoolConst(value: String) extends BoolOperatorsCAST with ConstTerm {

  override def mangledString(): String = value

  override def toString() : String ={
    if(value.compareTo("true") == 0)
      return "1"
    else{
      return "0"
    }
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitBoolConst, this)
  }

  override def size(): Int = 1

  override def topLevelString(): String = value

  override def smtString(): String = toString()

  override def myClone(): CAST = new BoolConst(value)

  override def hashCode(): Int = value.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[BoolConst])
      return false
    else{
      that.asInstanceOf[BoolConst].value.compareTo(value) == 0
    }
  }
}
abstract class ArithOperatorsCAST extends CAST{
  override def getSort(): CASTNodeKind = CASTNodeKind.ARITHMETIC_KIND
}
case class Plus(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+" + "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" + "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase,visitorBase.visitPlus, this)
  }
  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "+"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(+ "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Plus(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])

}
case class Minus(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{

  override def toString() : String ={
    "("+opr1.toString+" - "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" - "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase,visitorBase.visitMinus,this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "-"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(- "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Minus(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])

}
case class Div(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{
  override def toString() : String ={
    "("+opr1.toString+" / "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" / "+opr2.mangledString+")"
  }

  @throws(classOf[DivisionByZeroException])
  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase,visitorBase.visitDiv, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "/"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(/ "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Div(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])

}
case class Mult(opr1: ArithOperatorsCAST, opr2: ArithOperatorsCAST) extends ArithOperatorsCAST with BinOp{
  override def toString() : String ={
    "("+opr1.toString+" * "+opr2.toString+")"
  }

  override def mangledString() : String ={
    "("+opr1.mangledString+" * "+opr2.mangledString+")"
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitMult, this)
  }

  override def size(): Int = opr1.size() + opr2.size()+ 1

  override def topLevelString(): String = "*"

  override def getLeft(): CAST = opr1

  override def getRight(): CAST = opr2

  override def smtString(): String = "(* "+opr1.smtString+" "+opr2.smtString+")"

  override def myClone(): CAST = new Mult(opr1.myClone().asInstanceOf[ArithOperatorsCAST], opr2.myClone().asInstanceOf[ArithOperatorsCAST])

}
case class IntVar(name: String) extends ArithOperatorsCAST with VarTerm{

  override def getName(): String = name

  override def mangledString(): String = {
    name
  }

  override def toString() : String ={
    name
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitIntVar, this)
  }

  override def size(): Int = 1

  override def topLevelString(): String = name

  override def smtString(): String = toString()

  override def myClone(): CAST = new IntVar(name)

  override def hashCode(): Int = name.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[IntVar])
      return false
    else{
      that.asInstanceOf[IntVar].name.compareTo(name) == 0
    }
  }
}
case class IntConst(value: BigInt) extends ArithOperatorsCAST with ConstTerm{

  override def toString() : String ={
    value.toString
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitIntConst, this)
  }

  override def size(): Int = 1

  override def topLevelString(): String = value.toString

  override def smtString(): String = toString()

  override def myClone(): CAST = new IntConst(value)

  override def hashCode(): Int = value.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[IntConst])
      return false
    else{
      that.asInstanceOf[IntConst].value.compareTo(value) == 0
    }
  }

  override def mangledString(): String = value.toString
}

case class LongConst(value: Long) extends ArithOperatorsCAST with ConstTerm{

  override def toString() : String ={
    value.toString
  }

  override def accept(visitorBase: CASTVisitorBase): VisitorReturnType = {
    doAccept(visitorBase, visitorBase.visitLongConst, this)
  }

  override def size(): Int = 1

  override def topLevelString(): String = value.toString

  override def smtString(): String = toString()

  override def myClone(): CAST = new LongConst(value)

  override def hashCode(): Int = value.hashCode

  override def equals(that: Any): Boolean = {
    if(!that.isInstanceOf[LongConst])
      return false
    else{
      that.asInstanceOf[LongConst].value.compareTo(value) == 0
    }
  }

  override def mangledString(): String = value.toString
}
