package backend.solver.coreast

abstract class CASTVisitorBase{

  def preVisit(node: CAST): (VisitorReturnType, Boolean)
  def postVisit(node: CAST): Unit = {}

  def visitGreater(greater: Greater): VisitorReturnType

  def visitLessThan(lessThan: LessThan): VisitorReturnType

  def visitLTE(lTE: LTE): VisitorReturnType

  def visitGTE(gTE: GTE): VisitorReturnType

  @throws(classOf[DivisionByZeroException])
  def visitEqual(equal: Equal): VisitorReturnType

  def visitAnd(and: And): VisitorReturnType

  def visitOr(or: Or): VisitorReturnType

  def visitNot(not: Not): VisitorReturnType

  def visitImplies(implies: Implies): VisitorReturnType

  def visitBoolVar(boolVar: BoolVar): VisitorReturnType

  def visitBoolConst(boolConst: BoolConst): VisitorReturnType

  def visitPlus(plus: Plus): VisitorReturnType

  def visitMinus(minus: Minus): VisitorReturnType

  @throws(classOf[DivisionByZeroException])
  def visitDiv(div: Div): VisitorReturnType

  def visitMult(mult: Mult): VisitorReturnType

  def visitIntConst(intConst: IntConst): VisitorReturnType

  def visitLongConst(intConst: LongConst): VisitorReturnType

  def visitIntVar(intVar: IntVar): VisitorReturnType
}
