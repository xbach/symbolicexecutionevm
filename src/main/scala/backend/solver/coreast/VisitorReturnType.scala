package backend.solver.coreast

case class DivisionByZeroException(message: String) extends Exception(message)

trait VisitorReturnType{
  def eq(v2: VisitorReturnType): Boolean
}


/*case class Z3ASTReturnType(ast: Z3AST) extends VisitorReturnType{
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)
  def getAST(): Z3AST = ast
}
case class Z3ASTJavaRetType(ast: Expr) extends VisitorReturnType{
  override def eq(v2: VisitorReturnType): Boolean = this.equals(v2)
  def getAST(): Expr = ast
}*/
