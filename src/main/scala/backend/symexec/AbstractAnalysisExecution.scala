package backend.symexec

import frontend.ast._

trait AnalysisRet extends VisitorRet

abstract class AbstractAnalysisExecution[E <: AnalysisRet] extends VisitorBase {
  
  override def visitEnd(op: Opcode, state: ProgramState): E

  override def visitSuicide(op: Opcode, state: ProgramState): E

  override def visitInvalid(op: Opcode, state: ProgramState): E

  override def visitCallStatic(op: Opcode, state: ProgramState): E

  override def visitTxexecGas(op: Opcode, state: ProgramState): E

  override def visitStateRoot(op: Opcode, state: ProgramState): E

  override def visitSSize(op: Opcode, state: ProgramState): E

  override def visitSStoreBytes(op: Opcode, state: ProgramState): E

  override def visitSLoadBytes(op: Opcode, state: ProgramState): E

  override def visitSSizeExt(op: Opcode, state: ProgramState): E

  override def visitRngSeed(op: Opcode, state: ProgramState): E

  override def visitBreakpoint(op: Opcode, state: ProgramState): E

  override def visitDelegateCall(op: Opcode, state: ProgramState): E

  override def visitAssertFail(op: Opcode, state: ProgramState): E

  override def visitRevert(op: Opcode, state: ProgramState): E

  override def visitReturn(op: Opcode, state: ProgramState): E

  override def visitCallCode(op: Opcode, state: ProgramState): E

  override def visitCall(op: Opcode, state: ProgramState): E

  override def visitCreate(op: Opcode, state: ProgramState): E

  override def visitSStoreBytesExt(op: Opcode, state: ProgramState): E

  override def visitSLoadBytesExt(op: Opcode, state: ProgramState): E

  override def visitSStoreExt(op: Opcode, state: ProgramState): E

  override def visitSLoadExt(op: Opcode, state: ProgramState): E

  override def visitJumDest(op: Opcode, state: ProgramState): E

  override def visitGas(op: Opcode, state: ProgramState): E

  override def visitMSize(op: Opcode, state: ProgramState): E

  override def visitPC(op: Opcode, state: ProgramState): E

  override def visitJumpi(op: Opcode, state: ProgramState): E

  override def visitJump(op: Opcode, state: ProgramState): E

  override def visitSStore(op: Opcode, state: ProgramState): E

  override def visitSLoad(op: Opcode, state: ProgramState): E

  override def visitMStore8(op: Opcode, state: ProgramState): E

  override def visitMStore(op: Opcode, state: ProgramState): E

  override def visitMLoad(op: Opcode, state: ProgramState): E

  override def visitPop(op: Opcode, state: ProgramState): E

  override def visitGasLimit(op: Opcode, state: ProgramState): E

  override def visitDifficulty(op: Opcode, state: ProgramState): E

  override def visitNumber(op: Opcode, state: ProgramState): E

  override def visitTimeStamp(op: Opcode, state: ProgramState): E

  override def visitCoinBase(op: Opcode, state: ProgramState): E

  override def visitBlockHash(op: Opcode, state: ProgramState): E

  override def visitMCopy(op: Opcode, state: ProgramState): E

  override def visitExtCodeCopy(op: Opcode, state: ProgramState): E

  override def visitExtCodeSize(op: Opcode, state: ProgramState): E

  override def visitGasPrice(op: Opcode, state: ProgramState): E

  override def visitCodeCopy(op: Opcode, state: ProgramState): E

  override def visitCodeSize(op: Opcode, state: ProgramState): E

  override def visitCallDataCopy(op: Opcode, state: ProgramState): E

  override def visitCallDataSize(op: Opcode, state: ProgramState): E

  override def visitCallDataLoad(op: Opcode, state: ProgramState): E

  override def visitCallValue(op: Opcode, state: ProgramState): E

  override def visitCaller(op: Opcode, state: ProgramState): E

  override def visitOrigin(op: Opcode, state: ProgramState): E

  override def visitBalance(op: Opcode, state: ProgramState): E

  override def visitAddress(op: Opcode, state: ProgramState): E

  override def visitSha3(op: Opcode, state: ProgramState): E

  override def visitByte(op: Opcode, state: ProgramState): E

  override def visitNot(op: Opcode, state: ProgramState): E

  override def visitXor(op: Opcode, state: ProgramState): E

  override def visitOr(op: Opcode, state: ProgramState): E

  override def visitAnd(op: Opcode, state: ProgramState): E

  override def visitIsZero(op: Opcode, state: ProgramState): E

  override def visitEq(op: Opcode, state: ProgramState): E

  override def visitSgt(op: Opcode, state: ProgramState): E

  override def visitSlt(op: Opcode, state: ProgramState): E

  override def visitGt(op: Opcode, state: ProgramState): E

  override def visitLt(op: Opcode, state: ProgramState): E

  override def visitSignExtEnd(op: Opcode, state: ProgramState): E

  override def visitExp(op: Opcode, state: ProgramState): E

  override def visitMulMod(op: Opcode, state: ProgramState): E

  override def visitAddMod(op: Opcode, state: ProgramState): E

  override def visitSMod(op: Opcode, state: ProgramState): E

  override def visitMod(op: Opcode, state: ProgramState): E

  override def visitSDIV(op: Opcode, state: ProgramState): E

  override def visitDiv(op: Opcode, state: ProgramState): E

  override def visitSub(op: Opcode, state: ProgramState): E

  override def visitMul(op: Opcode, state: ProgramState): E

  override def visitAdd(op: Opcode, state: ProgramState): E

  override def visitSwap(op: Opcode, state: ProgramState): E

  override def visitDup(op: Opcode, state: ProgramState): E

  override def visitLog(op: Opcode, state: ProgramState): E

  override def visitPush(op: Opcode, state: ProgramState): E

  override def visitStop(op: Opcode, state: ProgramState): E

  override def visitInstruction(instruction: Instruction, state: ProgramState): E

  override def visitInstructionList(instructionList: InstructionList, state: ProgramState): E

  override def visitTerminalBlock(basicBlock: BasicBlock, state: ProgramState): E

  override def visitConditionalBlock(basicBlock: BasicBlock, state: ProgramState): E

  override def visitUnconditionalBlock(basicBlock: BasicBlock, state: ProgramState): E

  override def visitBasicBlocks(basicBlockList: BasicBlocksList, state: ProgramState): E
}
