package backend.symexec

class Generator {
  def gen_gas_var(): String = {
    count += 1
    return "gas_" + count
  }


  def gen_gas_price_var(): String = return "Ip"

  def gen_balance_var(): String = {
    count += 1
    return "balance_" + count
  }

  def gen_mem_var(address: StackItem): String = {
    return "mem_" + address
  }

  private var count = 0

  def gen_arbitrary_var(): String = {
    count += 1
    return "some_var_" + count
  }


  def gen_data_size(): String = {
    return "Id_size"
  }

  var countData = 0

  def gen_data_var(): String = {
    countData += 1
    return "Id_" + countData.toString()
  }

  def gen_owner_store_var(position: StackItem): String = {
    return "Ia_store_" + position
  }

  def gen_arbitrary_address_var(): String = {
    count += 1
    return "some_address_" + count
  }
}
