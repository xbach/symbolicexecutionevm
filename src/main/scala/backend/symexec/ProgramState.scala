package backend.symexec

import com.microsoft.z3.{BitVecExpr, BoolExpr, Context, Expr}
import com.typesafe.scalalogging.Logger
import frontend.config.MyConfig
import utils.Stack

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

case class ProgramState() {

  val balance: mutable.HashMap[String, StackItem] = new mutable.HashMap[String, StackItem]()

  var funcCall: BigInt = -1

  def init(z3: Context, gen: Generator) = {
    var senderAddress: StackItem = null
    var receiverAddress: StackItem = null
    var depositedValue: StackItem = null
    var initIs: StackItem = null
    var initIa: StackItem = null

    if(MyConfig.config.inputState){

    } else {
      senderAddress = FormulaItem(z3.mkBVConst("Is", 256))
      receiverAddress = FormulaItem(z3.mkBVConst("Ia", 256))
      depositedValue = FormulaItem(z3.mkBVConst("Iv", 256))
      initIs = FormulaItem(z3.mkBVConst("init_Is", 256))
      initIa = FormulaItem(z3.mkBVConst("init_Ia", 256))
    }

    if(gasPrice == null){
      val newVarName = gen.gen_gas_price_var()
      gasPrice = FormulaItem(z3.mkBVConst(newVarName, 256))
      variables += newVarName -> gasPrice.getBVExpr(z3)
    }

    if(currentCoinBase == null){
      currentCoinBase = FormulaItem(z3.mkBVConst("IH_c", 256))
      variables += "IH_c" -> currentCoinBase.getBVExpr(z3)
    }

    if(currentTimeStamp== null){
      currentTimeStamp = FormulaItem(z3.mkBVConst("IH_s", 256))
      variables += "IH_s" -> currentTimeStamp.getBVExpr(z3)
    }

    if(currentDifficulty== null){
      currentDifficulty = FormulaItem(z3.mkBVConst("IH_d", 256))
      variables += "IH_d" -> currentDifficulty.getBVExpr(z3)
    }

    if(currentNumber== null){
      currentNumber = FormulaItem(z3.mkBVConst("IH_i", 256))
      variables += "IH_i" -> currentNumber.getBVExpr(z3)
    }

    if(currentGasLimit== null){
      currentGasLimit = FormulaItem(z3.mkBVConst("IH_l", 256))
      variables += "IH_l" -> currentGasLimit.getBVExpr(z3)
    }

    Is = senderAddress
    Ia = receiverAddress
    value = depositedValue

    variables += "Is" -> senderAddress.getBVExpr(z3)
    variables += "Ia" -> receiverAddress.getBVExpr(z3)
    variables += "Iv" -> depositedValue.getBVExpr(z3)

    //TODO: signed or unsigned?
    pathCondition.append(z3.mkBVSGE(depositedValue.getBVExpr(z3), z3.mkBV(0, 256)))
    pathCondition.append(z3.mkBVSGE(initIs.getBVExpr(z3), depositedValue.getBVExpr(z3)))
    pathCondition.append(z3.mkBVSGE(initIa.getBVExpr(z3), z3.mkBV(0, 256)))

    balance.update("Is", FormulaItem(z3.mkBVSub(initIs.getBVExpr(z3), depositedValue.getBVExpr(z3))))
    balance.update("Ia", FormulaItem(z3.mkBVAdd(initIa.getBVExpr(z3), depositedValue.getBVExpr(z3))))

  }

  // Sender address
  private var Is: StackItem = null

  // Receiver address
  private var Ia: StackItem = null

  private var origin: StackItem = null

  def getOrigin(): StackItem = origin

  def getSenderAddressIs() = Is

  def getReceiverAddressIa() = Ia

  private var gasPrice: StackItem = null

  def getGasPrice(): StackItem = gasPrice

  private var currentCoinBase: StackItem = null

  def getCurrentCoinBase() = currentCoinBase

  private var currentTimeStamp: StackItem = null

  def getCurrentTimeStamp() = currentTimeStamp

  private var currentNumber: StackItem = null

  def getCurrentNumber(): StackItem = currentNumber

  private var currentDifficulty: StackItem = null

  def getCurrentDifficulty(): StackItem = currentDifficulty

  private var currentGasLimit: StackItem = null

  def getCurrentGasLimit(): StackItem = currentGasLimit

  val sha3List = new mutable.HashMap[String, StackItem]()

  // Receiver addresses -> stored value. This is the storage for Ia
  val Ia_storage = new mutable.HashMap[String, StackItem]()

  val logger = Logger("ProgramState")

  var memory: List[StackItem] = List[StackItem]()

  val mem: mutable.HashMap[StackItem, StackItem] = new mutable.HashMap[StackItem, StackItem]()

  var miu_i: StackItem = IntItem(0)

  var pc: BigInt = 0

  def increasePC(): Unit = pc += 1

  var totalNumPaths: Int = 0

  val stack = new Stack[StackItem]

  var value: StackItem = IntItem(0) // deposited value

  val pathCondition = new ArrayBuffer[BoolExpr]()

  val variables = new mutable.HashMap[String, BitVecExpr]()

  val calls = new mutable.HashMap[BigInt, Boolean]()

  def printPathCondition(): Unit = {
    logger.info("Path Condition: "+pathCondition)
  }

  def copy(): ProgramState = {
    logger.warn("Remember to copy all variables in state ...")
    val cpState = new ProgramState
    cpState.Is = this.Is
    cpState.Ia = this.Ia
    cpState.origin = this.origin
    cpState.currentGasLimit = this.currentGasLimit
    cpState.currentDifficulty = this.currentDifficulty
    cpState.currentNumber = this.currentNumber
    cpState.currentTimeStamp = this.currentTimeStamp
    cpState.currentCoinBase = this.currentCoinBase
    cpState.gasPrice = this.gasPrice
    this.balance.foreach(f => cpState.balance += f)
    this.calls.foreach(f => cpState.calls += f)

    this.Ia_storage.foreach(f => cpState.Ia_storage += f )
    this.sha3List.foreach(s => cpState.sha3List += s)

    cpState.pc = this.pc
    cpState.totalNumPaths = this.totalNumPaths
    cpState.memory = this.memory.foldLeft(List[StackItem]()){(res, m) => res :+ m}
    cpState.miu_i = this.miu_i
    this.mem.foreach{case (k, v) => cpState.mem += k -> v}
    this.pathCondition.foreach(p => cpState.pathCondition.append(p))
    this.variables.foreach(v => cpState.variables += v)

    cpState.value = this.value
    this.stack.copy(cpState.stack)
    cpState
  }
}

