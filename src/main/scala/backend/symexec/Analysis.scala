package backend.symexec

import com.microsoft.z3.enumerations.Z3_decl_kind
import com.microsoft.z3._
import com.typesafe.scalalogging.Logger
import frontend.ast.GCost.Gcallstipend
import frontend.ast.Instruction
import frontend.ast.Opcode.{CALL, SUICIDE}

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

case class Analysis() {
  var moneyConcurrencyBug = new ArrayBuffer[BigInt]()

  val reentrancyBug = new ArrayBuffer[Boolean]()

  val moneyFlow = new ArrayBuffer[(String, String, String)]()

  var gas = 0
}

object Analysis {

  val logger = Logger("Analysis")

  private def getVars(exp: Expr): scala.collection.mutable.HashSet[BitVecExpr] ={
    val ret = new mutable.HashSet[BitVecExpr]()
    def collect(f: Expr): Unit = {
      if(f.isConst){
        if(f.getFuncDecl.getDeclKind == Z3_decl_kind.Z3_OP_UNINTERPRETED && !ret.contains(f.asInstanceOf[BitVecExpr]))
          ret.add(f.asInstanceOf[BitVecExpr])
      } else {
        f.getArgs.foreach(arg => collect(arg))
      }
    }
    collect(exp)
    ret
  }

  def checkReentrancyBug(state: ProgramState, context: Context): Boolean = {
    val solver = context.mkSolver()
    val newPathCond = new ArrayBuffer[BoolExpr]()
    state.pathCondition.foreach(p => {
      //solver.add(p)
      val listVars = getVars(p)
      listVars.foreach(v => {
        val varName = v.getFuncDecl.getName.toString
        // check if global var
        if(varName.startsWith("Ia_store")){
          val storageKey = if(varName.contains("-")) varName.split("-")(1) else varName.split("Ia_store_")(1)
          val realKey = storageKey /*try{
            "IntItem("+ BigInt(storageKey, 256).toString(16) +")"
          } catch {
            case _: Throwable => {
              "FormulaItem("+storageKey+")"
            }
          }*/
          val storageValue = state.Ia_storage.getOrElse(realKey, null)
          if(storageValue != null){
            val exp = context.mkEq(v, storageValue.getBVExpr(context))
            //solver.add(exp)
            newPathCond.append(exp)
          }
        }
      })
    })

    val transferAmount = state.stack.atPos(2)
    if(transferAmount.isSymbolic() && transferAmount.asInstanceOf[FormulaItem].formula.toString.contains("Ia_store")){
      val amountExp = transferAmount.asInstanceOf[FormulaItem].formula.toString
      val storageKey = if(amountExp.contains("-")) amountExp.split("-")(1) else amountExp.split("Ia_store_")(1)
      val realKey = storageKey.replace("|","") /*try{
        "IntItem("+ BigInt(storageKey, 256).toString(16) +")"
      } catch {
        case _: Throwable => {
          "FormulaItem("+storageKey+")"
        }
      }*/
      val storageValue = state.Ia_storage.getOrElse(realKey, null)
      if(storageValue != null){
        val exp = context.mkNot(context.mkEq(context.mkBV(0, 256), storageValue.getBVExpr(context)))
        //solver.add(exp)
        newPathCond.append(exp)
      }
    }
    state.pathCondition.foreach(p => solver.add(p))
    newPathCond.foreach(p => solver.add(p))

    println("****DEBUG")
    println(state.pathCondition)
    println("----------")
    println(newPathCond)
    println("***END DEBUG")
    val outGas = state.stack.atPos(0)
    val exp = context.mkBVSGT(outGas.getBVExpr(context), context.mkBV(Gcallstipend.cost, 256))
    solver.add(exp)
    val ret = !(solver.check() == Status.UNSATISFIABLE)
    solver.getUnsatCore.foreach(f => logger.debug(f.toString))
    logger.debug("RE-ENTRANCY BUG? " + ret)
    ret
  }

  def updateAnalysis(analysis: Analysis, instruction: Instruction, state: ProgramState, solver: Solver, context: Context): Unit = {
    //TODO: calculate gas
    instruction.getOpCode() match {
      case CALL => {
        val recipient = state.stack.atPos(1).simplify()
        val transferAmount = state.stack.atPos(2)
        if(transferAmount.isReal() && transferAmount.getNumeralValue().get == 0) {
          return
        }
        val reentrancyResult = checkReentrancyBug(state, context)
        analysis.reentrancyBug.append(reentrancyResult)
        analysis.moneyConcurrencyBug.append(state.pc)
        analysis.moneyFlow.append(("Ia", recipient.toString, transferAmount.toString))
      }
      case SUICIDE => {
        val recipient = state.stack.atPos(0).simplify()
        analysis.moneyConcurrencyBug.append(state.pc)
        analysis.moneyFlow.append(("Ia", recipient.toString, "all_remaining"))
      }
      case _ => {}
    }
  }
}
