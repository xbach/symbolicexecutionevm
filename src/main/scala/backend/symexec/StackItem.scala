package backend.symexec

import java.math.BigInteger

import com.microsoft.z3._
import com.typesafe.scalalogging.Logger

abstract class StackItem{
  def isSymbolic(): Boolean

  def isReal(): Boolean = !isSymbolic()

  def getNumeralValue(): Option[BigInt]

  def simplify(): StackItem

  def toUnsigned(): BigInt

  def toSigned(): BigInt

  def toSymbolic(z3: Context): FormulaItem

  def getBVExpr(z3: Context): BitVecExpr
}
case class IntItem(value: BigInt) extends StackItem {
  lazy val x = BigInt(BigInteger.ONE.shiftLeft(256))

  val logger = Logger("IntItem: " + value)

  def isSymbolic(): Boolean = false

  def getNumeralValue(): Option[BigInt] = Some(value)

  override def simplify(): IntItem = this

  override def toUnsigned(): BigInt = {
    val n = value
    if(n.bigInteger.signum() < 0) {
      n + x
    }
    else n
  }

  def toSigned(): BigInt = {
    val n = value
    if(n.bigInteger.signum() > math.pow(2, 256 - 1)){
      //throw new RuntimeException("Conversion to signed not yet implemented")
      logger.warn("Called toSigned.....")
      (BigInt(math.pow(2, 256).toLong) - n) * (-1)
    }
    n
  }

  override def toSymbolic(z3: Context): FormulaItem = {
    //val dec = BigDecimal(value)
    //val bv = z3.mkBV(dec.toString(), 256)
    val bv = z3.mkBV(value.toString(), 256)
    FormulaItem(bv)
  }

  //override def toString: String = value.toString()

  override def getBVExpr(z3: Context): BitVecExpr = z3.mkBV(value.toString(), 256)
}
case class FormulaItem(formula: Expr) extends StackItem {

  override def getBVExpr(z3: Context): BitVecExpr = formula.asInstanceOf[BitVecExpr]

  //override def toString: String = formula.toString

  def isSymbolic(): Boolean = {
    !formula.simplify().isNumeral
  }

  def getNumeralValue(): Option[BigInt] = {
    formula match {
      case bv: BitVecNum => Some(BigInt(bv.getBigInteger))
      case i: IntNum => Some(BigInt(i.getBigInteger))
      case _ => None
    }
  }

  override def simplify(): FormulaItem = FormulaItem(formula.simplify())

  override def toUnsigned(): BigInt = throw new RuntimeException("Should not call this for Formula type")

  override def toSymbolic(z3: Context): FormulaItem = this

  override def toSigned(): BigInt = ???
}
