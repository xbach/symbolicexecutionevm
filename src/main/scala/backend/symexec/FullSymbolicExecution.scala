package backend.symexec

import java.util

import com.microsoft.z3._
import utils.{Stack, Utils}
import com.typesafe.scalalogging.Logger
import frontend.ast.Opcode.{ASSERTFAIL, CALL, INVALID}
import frontend.ast._
import frontend.config.{INPUT_TYPE, MyConfig}
import frontend.parser.Input
import org.bouncycastle.jcajce.provider.digest.SHA3
import org.bouncycastle.util.encoders.Hex

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

sealed trait ProblematicPCInfo
case class AssertionError(pc: BigInt, model: Model) extends ProblematicPCInfo
case class ReentrancyBug(pc: BigInt) extends ProblematicPCInfo

trait SymbolicExecRet extends AnalysisRet
case class TerminatePath() extends SymbolicExecRet
case class NextInstruction() extends SymbolicExecRet
case class NextBlockUnconditional(nextBlkAdrr: BigInt) extends SymbolicExecRet
case class NextBlockConditional(nextBlkAddr: BigInt, branchCond: BoolExpr) extends SymbolicExecRet
case class TerminateAnalysis() extends SymbolicExecRet

case class DirectedEdge(s: BigInt, d: BigInt)

class FullSymbolicExecution(input: Input) extends AbstractAnalysisExecution[SymbolicExecRet] {
  val analysis = new Analysis
  val cfg = new util.HashMap[String, String]()
  val z3: Context = new Context(cfg)
  val solver = z3.mkSolver()

  val ONEBV = z3.mkBV(1, 256)
  val ZEROBV = z3.mkBV(0, 256)
  val CONSTANT_ONES_159 = z3.mkBV((1 << 160) - 1, 256)

  lazy val EXP2256 = BigDecimal.valueOf(math.pow(2, 256)).toBigInt()

  val problematicPCs = new mutable.HashMap[String, Array[_<:ProblematicPCInfo]]()

  val callsAffectState = new mutable.HashMap[BigInt, Boolean]()

  val recipients = new mutable.HashSet[String]()

  val generator = new Generator()

  var numberOfTests = 0

  var MSIZE = false

  val logger = Logger(classOf[FullSymbolicExecution])

  def init(): Unit ={
    logger.debug("Start symbolic execution")
    if(scala.io.Source.fromFile(input.disasmFile).mkString.contains("MSIZE"))
      MSIZE = true
  }

  init()
  override def visitBasicBlocks(basicBlockList: BasicBlocksList, state: ProgramState): SymbolicExecRet = {
    state.init(z3, generator)
    val visitedEdge = new scala.collection.mutable.HashMap[DirectedEdge, Int]()

    def symExecBlk(state: ProgramState, curBlkAddr: BigInt, prevBlkAddr: BigInt, depth: Int): Unit = {
      val edge = DirectedEdge(prevBlkAddr, curBlkAddr)
      val count = visitedEdge.getOrElseUpdate(edge, 0)
      val newCount = count + 1
      visitedEdge.update(edge, newCount)

      if(newCount > MyConfig.config.loopLimit){
        logger.debug("Reached loop limit ...")
        logger.debug("Terminating this path ...")
        return
      }

      //TODO: Check GAS used here, reentrancy analysis
      if(analysis.gas > MyConfig.config.gasLimit){
        logger.debug("Reached gas limit ...")
        logger.debug("Terminating this path ...")
        return
      }

      logger.debug("Now visiting block: "+ curBlkAddr)

      val blk = basicBlockList.basicBlocks.getOrElse(curBlkAddr, null)

      if(blk != null)
        logger.debug("End block: "+ blk.getInstructionList().insts.last.getAddr())

      if(blk == null)
        throw new RuntimeException("Not found addr: "+curBlkAddr)
      val res = blk.accept(this, state)

      val nextDepth = depth + 1

      if(res.isInstanceOf[TerminatePath] || nextDepth > MyConfig.config.depthLimit){
        logger.debug("Terminating a path ...")
        //logger.debug("Path condition: ")
        //state.pathCondition.foreach(p => {
        //  logger.debug(p.simplify().toString)
        //})

        state.printPathCondition()
        numberOfTests += 1
        if(MyConfig.config.genTestCases){
          val model = solver.getModel
          val res = new ArrayBuffer[BitVecNum]()
          state.variables.values.foreach(v => {
            val x = model.eval(v, false)
            if(x.isInstanceOf[BitVecNum]){
              res.append(x.asInstanceOf[BitVecNum])
              logger.debug(v+" "+x.asInstanceOf[BitVecNum].toString)
            }
          })
          if(res.size == 2){
            solver.push()
            solver.add(z3.mkBVSGT(z3.mkBVSub(res(1), res(0)), z3.mkBV(3, 256)))
            val sat = solver.check()
            logger.debug(sat.toString)
            solver.pop()
          }

          //logger.debug(model.toString)
        }
      } else {
        res match {
          case NextBlockUnconditional(nextBlkAddr) => {
            //logger.debug("Visited unconditional block, last: "+state.pc +" next:" + nextBlkAddr)

            val cpState = state //.copy()
            cpState.pc = nextBlkAddr
            // falls to => no need below code for function call
            // falls to when next block address is equal to current pc
            if(input.sourceMap != null && state.pc != nextBlkAddr){
              val sourceCode = input.sourceMap.getSourceCode(state.pc)
              if(input.sourceMap.funcCallNames.contains(sourceCode)){
                cpState.funcCall = state.pc
              }
            }
            symExecBlk(cpState, nextBlkAddr, curBlkAddr, nextDepth)
          }
          case NextBlockConditional(nextBlkAddr, branchCond) => {
            //logger.debug("Visited conditional block: then branch, current: "+curBlkAddr+" last: "+state.pc +" next: "+nextBlkAddr)
            //logger.debug("Branch expression: "+ branchCond)
            var thenInfeasible = false
            solver.push()
            solver.add(branchCond)
            if(solver.check() == Status.UNSATISFIABLE){
              logger.info("then: INFEASIBLE PATH DETECTED!")
              thenInfeasible = true
              solver.pop()
            } else {
              solver.pop()
              val cpState = state.copy()
              //logger.debug("****STACK then branch: ")
              //cpState.stack.print()
              cpState.pc = nextBlkAddr
              cpState.pathCondition.append(branchCond)
              cpState.printPathCondition()
              symExecBlk(cpState, nextBlkAddr, curBlkAddr, nextDepth)
            }

            val negatedBranchCond = z3.mkNot(branchCond)
            var processElse = true
            if(!thenInfeasible) {
              solver.push()
              solver.add(negatedBranchCond)
              if(solver.check() == Status.UNSATISFIABLE){
                logger.info("else: INFEASIBLE PATH DETECTED!")
                processElse = false
              }
            }
            if(processElse) {
              if(!thenInfeasible)
                solver.pop()
              val cpState2 = state.copy()
              cpState2.pc = basicBlockList.staticEdges.get(state.pc).get
              logger.debug("Visited conditional block: else branch," /*+ curBlkAddr + " end: " + (cpState2.pc - 1)*/ + " next: " + cpState2.pc)
              //logger.debug("****STACK else branch: ")
              //cpState2.stack.print()
              cpState2.pathCondition.append(negatedBranchCond)
              cpState2.printPathCondition()
              symExecBlk(cpState2, cpState2.pc, curBlkAddr, nextDepth)
            }

          }
        }
      }
    }
    symExecBlk(state, 0, 0, 0)
    TerminateAnalysis()
  }

  override def visitTerminalBlock(basicBlock: BasicBlock, state: ProgramState): SymbolicExecRet = {
    basicBlock.asInstanceOf[TerminalBlock].instructionList.accept(this, state)
    TerminatePath()
  }

  override def visitConditionalBlock(basicBlock: BasicBlock, state: ProgramState): SymbolicExecRet = {
    basicBlock.asInstanceOf[ConditionalBlock].instructionList.accept(this, state).asInstanceOf[NextBlockConditional]
  }

  override def visitUnconditionalBlock(basicBlock: BasicBlock, state: ProgramState): SymbolicExecRet = {
    basicBlock.asInstanceOf[UnconditionalBlock].instructionList.accept(this, state) match {
      case n: NextBlockUnconditional => n
      case p: NextInstruction => NextBlockUnconditional(state.pc)
    }
  }

  override def preVisit(instruction: Program, state: ProgramState): (VisitorRet, Boolean) = {
    // Before visiting each instruction, update the analysis. We update analysis before visiting because
    // after visiting an instruction, state (stack) is already modified.
    if(instruction.isInstanceOf[Instruction]) {
      logger.debug("Previsiting: " + instruction)
      instruction.asInstanceOf[Instruction].getOpCode() match {
        case INVALID | ASSERTFAIL => {}
        case CALL => {
          Analysis.updateAnalysis(analysis, instruction.asInstanceOf[Instruction], state, solver, z3)
          if(!analysis.reentrancyBug.isEmpty && analysis.reentrancyBug.last) {
            val res = problematicPCs.getOrElse("ReentrancyBug", Array[ProblematicPCInfo]())
            problematicPCs.update("ReentrancyBug", res :+ ReentrancyBug(state.pc))
          }
        }
        case _ => {
          Analysis.updateAnalysis(analysis, instruction.asInstanceOf[Instruction], state, solver, z3)
        }
      }
    }
    (EmptyRet(), true)
  }

  override def visitInstructionList(instructionList: InstructionList, state: ProgramState): SymbolicExecRet = {
    var lastRet: VisitorRet = null
    instructionList.insts.foreach(instruction => {
      //logger.debug("****STACK: "+i +" "+instructionList.insts(i))
      //state.stack.print()
      lastRet = instruction.accept(this, state)
    })

    // Return the result of last instruction in the list
    // return instructionList.insts(i).accept(this, state).asInstanceOf[SymbolicExecRet]
    return lastRet.asInstanceOf[SymbolicExecRet]
  }

  override def visitInstruction(instruction: Instruction, state: ProgramState): SymbolicExecRet = {
    instruction.getOpCode() match {
      case _: PUSH => {
        val pushedValue = BigInt(instruction.asInstanceOf[Instruction1Arg].arg.replace("0x",""), 16)
        state.stack.push(IntItem(pushedValue))
        instruction.getOpCode().accept(this, state).asInstanceOf[SymbolicExecRet]
      }
      case _ => {
        instruction.getOpCode().accept(this, state).asInstanceOf[SymbolicExecRet]
      }
    }
  }

  override def visitJumDest(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    NextInstruction()
  }

  def checkStackOverUnderFlow(state: ProgramState): Unit = {
    if(state.stack.isEmpty())
      throw new RuntimeException("Stack Underflow")

    if(state.stack.size() > 1024)
      throw new RuntimeException("Stack Overflow (size cannot be greater than 1024)")
  }

  override def visitJumpi(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackOverUnderFlow(state)

    val nextAddress = state.stack.pop()
    val flag = state.stack.pop()
    var branchCond: BoolExpr = z3.mkEq(z3.mkBV(0, 1), z3.mkBV(1, 1)) // false
    if(flag.isReal()){
      if(flag.getNumeralValue().get != 0){
        branchCond = z3.mkEq(z3.mkBV(1, 1), z3.mkBV(1, 1)) // true
      }
    } else {
      branchCond = z3.mkNot(z3.mkEq(flag.asInstanceOf[FormulaItem].formula, z3.mkBV(0, 256)))
    }

    val simplified = nextAddress.simplify()

    simplified.getNumeralValue() match {
      case Some(addr) => NextBlockConditional(addr, branchCond.simplify().asInstanceOf[BoolExpr])
      case None => throw new RuntimeException("Target address must be an integer!")
    }
  }

  override def visitJump(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackOverUnderFlow(state)

    val nextAddress = state.stack.pop()
    val simp = nextAddress.simplify()

    simp.getNumeralValue() match {
      case Some(addr) => NextBlockUnconditional(addr)
      case None => throw new RuntimeException("Target address must be an integer!")
    }
  }

  override def visitPush(op: Opcode, state: ProgramState): SymbolicExecRet = {
    val position = op.toString.replace("PUSH","").toInt
    state.pc = state.pc + 1 + position
    NextInstruction()
  }

  // Boolean operators

  override def visitNot(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackOverUnderFlow(state)
    val top = state.stack.pop()

    if(top.isReal()){
      val dec = BigDecimal(top.getNumeralValue().get)
      val notBV = z3.mkBVNot(z3.mkBV(dec.toString(), 256)).simplify().asInstanceOf[BitVecNum]
      state.stack.push(IntItem(BigInt(notBV.getBigInteger)))
    } else {
      top.asInstanceOf[FormulaItem].formula match {
        case bv: BitVecExpr => {
          val notBV = z3.mkBVNot(bv).simplify().asInstanceOf[BitVecExpr]
          state.stack.push(FormulaItem(notBV))
        }
        case _ => throw new RuntimeException("Not expected bitvec not operator on: "+top)
      }
    }
    state.increasePC()
    NextInstruction()
  }

  override def visitXor(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 1)
    state.increasePC()
    val first = state.stack.pop().toSymbolic(z3)
    val second = state.stack.pop().toSymbolic(z3)
    val xor = z3.mkBVXOR(first.formula.asInstanceOf[BitVecExpr], second.formula.asInstanceOf[BitVecExpr])
    state.stack.push(FormulaItem(xor.simplify()))
    NextInstruction()
  }

  override def visitOr(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    val first = state.stack.pop().toSymbolic(z3)
    val second = state.stack.pop().toSymbolic(z3)
    val or = z3.mkBVOR(first.formula.asInstanceOf[BitVecExpr], second.formula.asInstanceOf[BitVecExpr])
    state.stack.push(FormulaItem(or.simplify()))
    NextInstruction()
  }

  override def visitAnd(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 1){
      state.increasePC()
      val first = state.stack.pop().toSymbolic(z3)
      val second = state.stack.pop().toSymbolic(z3)
      val and = z3.mkBVAND(first.formula.asInstanceOf[BitVecExpr], second.formula.asInstanceOf[BitVecExpr])
      state.stack.push(FormulaItem(and.simplify()))
    } else {
      throw new RuntimeException("Stack underflow")
    }

    NextInstruction()
  }

  override def visitEnd(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  //TODO: somewhat symbolic, may need to change when switched to concolic
  override def visitSuicide(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 1)
    state.increasePC()
    val recipient = state.stack.pop()
    val transferAmount = state.balance.get("Ia").get
    state.balance.update("Ia", IntItem(0))
    val newAddrName = if(recipient.isReal()) "concrete_address_" + recipient else generator.gen_arbitrary_address_var()
    val oldBalanceName = generator.gen_arbitrary_var()
    val oldBalance = z3.mkBVConst(oldBalanceName, 256)
    state.variables += oldBalanceName -> oldBalance
    val constraint = z3.mkBVSGE(oldBalance, ZEROBV)
    solver.add(constraint)
    state.pathCondition.append(constraint)
    val newBalance = z3.mkBVAdd(oldBalance, transferAmount.getBVExpr(z3))
    state.balance += newAddrName -> FormulaItem(newBalance)
    NextInstruction()
  }

  override def visitInvalid(op: Opcode, state: ProgramState): SymbolicExecRet = {
    NextInstruction()
  }

  override def visitCallStatic(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitTxexecGas(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitStateRoot(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSSize(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSStoreBytes(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSLoadBytes(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSSizeExt(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitRngSeed(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitBreakpoint(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  // Does not seem to be well implemented in Oyente
  override def visitDelegateCall(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 6)
    state.increasePC()
    state.stack.pop()
    val recipient = state.stack.pop()
    if(MyConfig.config.useGlobalStorage){
      if(recipient.isReal()){
        val recipientStr = recipient.getNumeralValue().get.toLong.toHexString
        if(recipientStr.last.compare('L') == 0){
          recipients.add(recipientStr.substring(0, recipientStr.length - 2))
        } else
          recipients.add(recipientStr)
      }
    }
    state.stack.pop()
    state.stack.pop()
    state.stack.pop()
    state.stack.pop()
    val newVarName = generator.gen_arbitrary_var()
    val newVar = z3.mkBVConst(newVarName, 256)
    state.stack.push(FormulaItem(newVar))
    NextInstruction()
  }

  override def visitAssertFail(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(input.sourceMap != null){
      val sourceCode = input.sourceMap.getSourceCode(state.pc)
      val funcName = sourceCode.split("(")(0).trim
      val model = if(Utils.checkSat(solver, false) != Status.UNSATISFIABLE) solver.getModel else null
      if(funcName.compareTo("assert") == 0) {
        val assertErrors = problematicPCs.getOrElse("AssertionError", Array[AssertionError]())
        val addedAssertErrors = assertErrors :+ AssertionError(state.pc, model)
        problematicPCs.update("AssertionError", addedAssertErrors)
      } else if(state.funcCall != -1){
        val assertErrors = problematicPCs.getOrElse("AssertionError", Array[AssertionError]())
        val addedAssertErrors = assertErrors :+ AssertionError(state.funcCall, model)
        problematicPCs.update("AssertionError", addedAssertErrors)
      }
    }
    NextInstruction()
  }

  override def visitRevert(op: Opcode, state: ProgramState): SymbolicExecRet = {
    //TODO: handle miu_i
    if(state.stack.size() > 1){
      state.increasePC()
      state.stack.pop()
      state.stack.pop()
    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitReturn(op: Opcode, state: ProgramState): SymbolicExecRet = {
    //TODO: handle miu_i
    if(state.stack.size() > 1){
      state.increasePC()
      state.stack.pop()
      state.stack.pop()
    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitCallCode(op: Opcode, state: ProgramState): SymbolicExecRet = {
    //TODO: need to handle miu_i
    checkStackUnderFlow(state, 7)
    state.increasePC()
    state.calls += state.pc -> true
    state.calls.keySet.foreach(callPC => {
      if(!callsAffectState.contains(callPC)){
        callsAffectState += callPC -> false
      }
    })
    val outGas = state.stack.pop()
    val recipient = state.stack.pop()
    if(MyConfig.config.useGlobalStorage){
      if(recipient.isReal()){
        val recipientStr = recipient.getNumeralValue().get.toLong.toHexString
        if(recipientStr.last.compare('L') == 0){
          recipients.add(recipientStr.substring(0, recipientStr.length - 2))
        } else
          recipients.add(recipientStr)
      }
    }
    val transferAmount = state.stack.pop()
    val startDataInput = state.stack.pop()
    val sizeDataInput = state.stack.pop()
    val startDataOutput = state.stack.pop()
    val sizeDataOutput = state.stack.pop()

    if(transferAmount.isReal()) {
      if (transferAmount.getNumeralValue().get == 0){
        state.stack.push(IntItem(1))
        return NextInstruction()
      }
    }

    val balanceIa = state.balance.get("Ia").get.getBVExpr(z3)
    val transferAmountExp = transferAmount.getBVExpr(z3)
    val isEnoughFund = z3.mkBVSLE(transferAmountExp, balanceIa)
    solver.push()
    solver.add(isEnoughFund)
    if(Utils.checkSat(solver) == Status.UNSATISFIABLE){
      // This means not enough fund
      solver.pop()
      state.stack.push(IntItem(0))
    } else {
      // The execution is possibly ok
      state.stack.push(IntItem(1))
      solver.pop()
      solver.add(isEnoughFund)
      state.pathCondition.append(isEnoughFund)
      //TODO: do something with time dependency bug analysis
    }
    NextInstruction()
  }

  override def visitCall(op: Opcode, state: ProgramState): SymbolicExecRet = {
    //TODO: need to handle miu_i
    checkStackUnderFlow(state, 7)
    state.increasePC()
    state.calls += state.pc -> true
    state.calls.keySet.foreach(callPC => {
      if(!callsAffectState.contains(callPC)){
        callsAffectState += callPC -> false
      }
    })
    val outGas = state.stack.pop()
    val recipient = state.stack.pop()
    val transferAmount = state.stack.pop()
    val startDataInput = state.stack.pop()
    val sizeDataInput = state.stack.pop()
    val startDataOutput = state.stack.pop()
    val sizeDataOutput = state.stack.pop()

    if(transferAmount.isReal()) {
      if (transferAmount.getNumeralValue().get == 0){
        state.stack.push(IntItem(1))
        return NextInstruction()
      }
    }

    val balanceIa = state.balance.get("Ia").get.getBVExpr(z3)
    val transferAmountExp = transferAmount.getBVExpr(z3)
    val isEnoughFund = z3.mkBVSLE(transferAmountExp, balanceIa)
    solver.push()
    solver.add(isEnoughFund)
    if(Utils.checkSat(solver) == Status.UNSATISFIABLE){
      // This means not enough fund
      solver.pop()
      state.stack.push(IntItem(0))
    } else {
      // The execution is possibly ok
      state.stack.push(IntItem(1))
      solver.pop()
      solver.add(isEnoughFund)
      state.pathCondition.append(isEnoughFund)
      //TODO: do something with time dependency bug analysis
      val newBalanceIa = z3.mkBVSub(balanceIa, transferAmountExp)
      state.balance.update("Ia", FormulaItem(newBalanceIa.simplify()))
      val addressIs = z3.mkBVAND(state.variables.get("Is").get, CONSTANT_ONES_159)
      val boolExp = z3.mkNot(z3.mkEq(addressIs, recipient.getBVExpr(z3)))
      solver.push()
      solver.add(boolExp)
      if(Utils.checkSat(solver) == Status.UNSATISFIABLE){
        solver.pop()
        val balanceIs = state.balance.get("Is").get.getBVExpr(z3)
        val newBalanceIs = z3.mkBVAdd(balanceIs, transferAmountExp)
        state.balance.update("Is", FormulaItem(newBalanceIs.simplify()))
      } else{
        solver.pop()
        val newAddressName = if(recipient.isReal()){"concrete_address_" + recipient} else generator.gen_arbitrary_address_var()
        val oldBalanceName = generator.gen_arbitrary_address_var()
        val oldBalance = z3.mkBVConst(oldBalanceName, 256)
        state.variables += oldBalanceName -> oldBalance
        //TODO: signed or unsigned?
        val constraint = z3.mkBVSGE(oldBalance, ZEROBV)
        solver.add(constraint)
        state.pathCondition.append(constraint)
        val newBalance = z3.mkBVAdd(oldBalance, transferAmountExp)
        state.balance += newAddressName -> FormulaItem(newBalance.simplify())
      }
    }
    NextInstruction()
  }

  override def visitCreate(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 3)
    state.increasePC()
    state.stack.pop()
    state.stack.pop()
    state.stack.pop()
    val newVarName = generator.gen_arbitrary_var()
    val newVar = z3.mkBVConst(newVarName, 256)
    state.stack.push(FormulaItem(newVar))
    NextInstruction()
  }

  override def visitSStoreBytesExt(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSLoadBytesExt(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSStoreExt(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSLoadExt(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitGas(op: Opcode, state: ProgramState): SymbolicExecRet = {
    // In general, we do not have this precisely. It depends on both
    // the initial gas and the amount has been depleted
    // we need o think about this in the future, in case precise gas
    // can be tracked
    state.increasePC()
    val new_var_name = generator.gen_gas_var()
    val new_var = z3.mkBVConst(new_var_name, 256)
    state.variables += new_var_name -> new_var
    state.stack.push(FormulaItem(new_var))
    NextInstruction()
  }

  override def visitMSize(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    val msize = z3.mkBVMul(state.miu_i.getBVExpr(z3), z3.mkBV(32, 256))
    state.stack.push(FormulaItem(msize.simplify()))
    NextInstruction()
  }

  override def visitPC(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.stack.push(IntItem(state.pc))
    state.increasePC()
    NextInstruction()
  }

  override def visitSStore(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 2){
      state.calls += state.pc -> true
      state.calls.keySet.foreach(callPC => callsAffectState += callPC -> true)

      state.increasePC()
      val stored_address = state.stack.pop()
      val stored_value = state.stack.pop()
      // Note that the stored_value could be unknown
      logger.debug("SSTORE: "+stored_address.toString +" -> "+stored_value.toString)
      state.Ia_storage.getOrElseUpdate(stored_address.toString, stored_value)

    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitSLoad(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 0){
      state.increasePC()
      val position = state.stack.pop()
      if(position.isReal() && state.Ia_storage.contains(position.toString)){
        val storageValue = state.Ia_storage.get(position.toString).get
        state.stack.push(storageValue)
      } else if(MyConfig.config.useGlobalStorage && position.isReal() && state.Ia_storage.contains(position.toString)){
        throw new NotImplementedError("Unimplemented feature for using global storage!")
      } else {
        if(state.Ia_storage.contains(position.toString)){
          val storageValue = state.Ia_storage.get(position.toString).get
          state.stack.push(storageValue)
        } else {
          val simpPosition = position.simplify()
          var newVarName = ""
          if(input.sourceMap != null){
            newVarName = input.sourceMap.getVarName(state.pc - 1)
            if(input.sourceMap.isParamOrStateVar(newVarName)){
              newVarName = input.sourceMap.getSourceCode(state.pc -1)
              newVarName = "Ia_store" + "-" + simpPosition + "-" + newVarName
            } else {
              newVarName = generator.gen_owner_store_var(position)
            }
          } else
            newVarName = generator.gen_owner_store_var(position)

          val newVar = state.variables.getOrElseUpdate(newVarName, z3.mkBVConst(newVarName, 256))
          state.stack.push(FormulaItem(newVar))
          state.Ia_storage.put(position.toString, FormulaItem(newVar))
        }
      }
    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitMStore8(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    val storedAddress = state.stack.pop()
    val tempValue = state.stack.pop()
    val storedValue = z3.mkBVSMod(tempValue.getBVExpr(z3), z3.mkBV(256, 256))
    var currentMiuI = state.miu_i
    if(storedAddress.isReal() && currentMiuI.isReal()){
      val realAddress = storedAddress.getNumeralValue().get
      val temp = math.ceil(((realAddress + 1).toFloat / 32.toFloat).toDouble)
      if(temp > currentMiuI.getNumeralValue().get.toDouble){
        currentMiuI = IntItem(BigInt(temp.toLong))
      }
      state.mem += storedAddress -> FormulaItem(storedValue)
    } else{
      //TODO: signed or unsigned?
      val temp = z3.mkBVSDiv(storedAddress.getBVExpr(z3), z3.mkBV(32, 256))
      val exp = z3.mkBVSLT(currentMiuI.getBVExpr(z3), temp)
      solver.push()
      solver.add(exp)
      if(MSIZE){
        if(Utils.checkSat(solver) == Status.UNSATISFIABLE){
          currentMiuI = FormulaItem(z3.mkITE(exp, temp, currentMiuI.getBVExpr(z3)))
        }
      }
      solver.pop()
      state.mem.clear()
      state.mem += storedAddress -> FormulaItem(storedValue)
    }
    state.miu_i = currentMiuI

    NextInstruction()
  }

  override def visitMStore(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 1){
      state.increasePC()
      val stored_address = state.stack.pop()
      val stored_value = state.stack.pop()
      var current_miu_i = state.miu_i

      if(stored_address.isReal()){
        val oldSize = Math.floorDiv(state.memory.length, 32) // By byte, thus, the division of 32 = 4 bytes = a word
        val newSize = Math.floorDiv(Utils.ceil32(stored_address.getNumeralValue().get + 32).toLong, 32)
        val memExtend = (newSize -oldSize) * 32
        state.memory =  state.memory ::: List.fill[IntItem](memExtend.toInt)(IntItem(0))
        var value = stored_value
        val addr = stored_address.getNumeralValue().get.toInt
        for (i <- (0 to 31).reverse){
          if(stored_value.isReal()){
            val v = value.getNumeralValue().get
            state.memory = state.memory.updated(addr + i, IntItem(v % 256))
            value = IntItem(v / 256)
          } else {
            // Stored value is symbolic expression
            value = value.simplify()
            value.getNumeralValue() match {
              case Some(v) => {
                state.memory = state.memory.updated(addr + i, IntItem(v % 256))
                value = IntItem(v / 256)
              }
              case None => {
                // Really symbolic expression
                val valueBV = value.asInstanceOf[FormulaItem].formula.asInstanceOf[BitVecExpr]
                val bv256 = z3.mkBV(256, 256)
                val u = z3.mkBVSMod(valueBV, bv256)
                state.memory = state.memory.updated(addr + i, FormulaItem(u).simplify())
                value = FormulaItem(z3.mkBVSDiv(valueBV, bv256)).simplify()
              }
            }
          }
        }
      } // stored address is not real

      if(stored_address.isReal() && current_miu_i.isReal()){
        val addr = stored_address.getNumeralValue().get
        val temp = BigInt(math.ceil((addr + 32).toFloat / float2Float(32)).toLong)
        val miu = current_miu_i.getNumeralValue().get

        if (temp > miu)
          current_miu_i = IntItem(temp)

        state.mem.update(stored_address, stored_value)  // note that the stored_value could be symbolic
      } else {
        //throw new RuntimeException("To investigate why stored address or current_miu_i is symbolic")
        val temp = z3.mkBVAdd(z3.mkBVSDiv(z3.mkBVAdd(stored_address.getBVExpr(z3), z3.mkBV(31, 256)), z3.mkBV(32, 256)), ONEBV)
        val expression = z3.mkBVSLT(current_miu_i.getBVExpr(z3), temp)
        solver.push()
        solver.add(expression)
        if(MSIZE){
          if(solver.check() != Status.UNSATISFIABLE){
            current_miu_i = FormulaItem(z3.mkITE(expression, temp, current_miu_i.getBVExpr(z3)))
          }
        }
        solver.pop()
        state.mem.clear()
        state.mem += stored_address -> stored_value
      }

      state.miu_i = current_miu_i

    } else {
      throw new RuntimeException("Stack under flow")
    }
    NextInstruction()
  }

  override def visitMLoad(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 0){
      state.increasePC()
      val address = state.stack.pop()
      var currentMiuI = state.miu_i
      if(address.isReal() && currentMiuI.isReal() && state.mem.contains(address)){
        val word = state.mem.get(address).get
        state.stack.push(word)
        val temp = Math.floorDiv(Utils.ceil32(address.getNumeralValue().get + 32).toLong, 32)
        if(temp > currentMiuI.getNumeralValue().get)
          state.miu_i = IntItem(BigInt(temp))
      } else {
        //throw new NotImplementedError("MLoad: Investigate why address or current_miu_i is symbolic")
        val temp = z3.mkBVAdd(z3.mkBVSDiv(z3.mkBVAdd(address.getBVExpr(z3), z3.mkBV(31, 256)), z3.mkBV(32, 256)), ONEBV)
        val expression = z3.mkBVSLT(currentMiuI.getBVExpr(z3), temp)
        solver.push()
        solver.add(expression)
        if(MSIZE){
          if(solver.check() != Status.UNSATISFIABLE){
            currentMiuI = FormulaItem(z3.mkITE(expression, temp, currentMiuI.getBVExpr(z3)))
          }
        }
        solver.pop()
        val newVarName = generator.gen_mem_var(address)
        val newVar = state.variables.getOrElseUpdate(newVarName, z3.mkBVConst(newVarName, 256))
        state.stack.push(FormulaItem(newVar))
        state.mem.update(address, FormulaItem(newVar))
      }

      state.miu_i = currentMiuI

    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitPop(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 0) {
      state.stack.pop()
      state.increasePC()
    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitGasLimit(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getCurrentGasLimit())
    NextInstruction()
  }

  override def visitDifficulty(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getCurrentDifficulty())
    NextInstruction()
  }

  override def visitNumber(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getCurrentNumber())
    NextInstruction()
  }

  override def visitTimeStamp(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getCurrentTimeStamp())
    NextInstruction()
  }

  override def visitCoinBase(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getCurrentCoinBase())
    NextInstruction()
  }

  //TODO: now this is completely symbolic, consider concolic execution as well
  override def visitBlockHash(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 1)
    logger.warn("Executing block hash opcode with complete symbolic execution!")
    state.increasePC()
    val newVar = state.variables.getOrElseUpdate("IH_blockhash", z3.mkBVConst("IH_blockhash", 256))
    state.stack.push(FormulaItem(newVar))
    NextInstruction()
  }

  override def visitMCopy(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitExtCodeCopy(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitExtCodeSize(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitGasPrice(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getGasPrice())
    NextInstruction()
  }

  override def visitCodeCopy(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitCodeSize(op: Opcode, state: ProgramState): SymbolicExecRet = {
    ???
  }

  override def visitCallDataCopy(op: Opcode, state: ProgramState): SymbolicExecRet = {
    //TODO: Don't know how to simulate this yet
    checkStackUnderFlow(state, 2)
    state.increasePC()
    state.stack.pop()
    state.stack.pop()
    state.stack.pop()
    NextInstruction()
  }

  override def visitCallDataSize(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    val genSizeVar = generator.gen_data_size()
    val sizeVar= state.variables.getOrElseUpdate(genSizeVar, z3.mkBVConst(genSizeVar, 256))
    state.stack.push(FormulaItem(sizeVar))
    NextInstruction()
  }

  /**
    * Load value for function's arguments: pop stack to get address where to load the data from
    * Note that the first four bytes are always preserved for function name so the real address
    * of the k(th) argument is always of 32 * k + 4
    * @param op
    * @param state
    * @return
    */
  override def visitCallDataLoad(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    if(state.stack.size()>0){
      val position = state.stack.pop()
      if (input.sourceMap != null) {
        val sourceCode = input.sourceMap.getSourceCode(state.pc - 1)
        if (sourceCode.startsWith("function") && position.isReal()) {
          val startIdx = sourceCode.indexOf("(") + 1
          val endIdx = sourceCode.indexOf(")")
          val paramNames = sourceCode.substring(startIdx, endIdx).split(",").foldLeft(Array[String]()) {
            (res, arg) => {
              res :+ arg.trim.split(" ")(1)
            }
          }
          // We are loading the k(th) argument. This is to find out k.
          val posOfArg = Math.floorDiv(position.getNumeralValue().get.toInt - 4, 32)
          val param = paramNames(posOfArg)
          input.sourceMap.varNames.append(param)
          val data = state.variables.getOrElseUpdate(param, z3.mkBVConst(param, 256))
          state.stack.push(FormulaItem(data))
        } else {
          val newVar = generator.gen_data_var()
          val data = state.variables.getOrElseUpdate(newVar, z3.mkBVConst(newVar, 256))
          state.stack.push(FormulaItem(data))
        }
      } else {// input is bytecode binary, source code not available
        val newVar = generator.gen_data_var()
        val data = state.variables.getOrElseUpdate(newVar, z3.mkBVConst(newVar, 256))
        state.stack.push(FormulaItem(data))
      }
    } else throw new RuntimeException("Stack underflow")
    NextInstruction()
  }

  override def visitCallValue(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.value)
    NextInstruction()
  }

  override def visitCaller(op: Opcode, state: ProgramState): SymbolicExecRet = {
    // get caller address
    // that is directly responsible for this execution
    state.increasePC()
    state.stack.push(state.getSenderAddressIs())
    NextInstruction()
  }

  override def visitOrigin(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getOrigin())
    NextInstruction()
  }

  override def visitBalance(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 1)
    state.increasePC()
    val address = state.stack.pop()
    if(address.isReal() && MyConfig.config.useGlobalStorage){
      throw new NotImplementedError("Not yet implemented use global storage in Balance")
    } else {
      val newVarName = generator.gen_balance_var()
      val newVar = state.variables.getOrElseUpdate(newVarName, z3.mkBVConst(newVarName, 256))
      val hashedAddress = if(address.isReal()) "concrete_address_" + address else address.toString
      state.balance.update(hashedAddress, FormulaItem(newVar))
      state.stack.push(FormulaItem(newVar))
    }
    NextInstruction()
  }

  override def visitAddress(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    state.stack.push(state.getReceiverAddressIa())
    NextInstruction()
  }

  //TODO: This implementation produces diff hash compared to oyente
  override def visitSha3(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    val s0 = state.stack.pop()
    val s1 = state.stack.pop()
    if(s0.isReal() && s1.isReal()){
      val v0 = s0.getNumeralValue().get.toInt
      val v1 = s1.getNumeralValue().get.toInt
      val data = state.memory.slice(v0, v0 + v1).foldLeft(""){(res, v) => res+v}.replaceAll("\\s","")
      val md = new SHA3.DigestSHA3(256) //same as DigestSHA3 md = new SHA3.Digest256();
      md.update(data.getBytes())
      val digest = md.digest
      val hexStr = Hex.toHexString(digest)
      val newVar = state.sha3List.getOrElseUpdate(hexStr, FormulaItem(z3.mkBVConst(generator.gen_arbitrary_var(), 256)))
      state.stack.push(newVar)
    } else {
      val newName = generator.gen_arbitrary_var()
      val newVar = z3.mkBVConst(newName, 256)
      state.variables.update(newName, newVar)
      state.stack.push(FormulaItem(newVar))
    }
    NextInstruction()
  }

  override def visitByte(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitIsZero(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 0){
      state.increasePC()
      val top = state.stack.pop()
      if(top.isReal()){
        val value = top.getNumeralValue().get
        if(value == 0)
          state.stack.push(IntItem(1))
        else
          state.stack.push(IntItem(0))
      } else {
        val topSumb = top.toSymbolic(z3)
        val cond = z3.mkEq(topSumb.formula, ZEROBV)
        val ite = z3.mkITE(cond, ONEBV, ZEROBV).simplify()
        state.stack.push(FormulaItem(ite))
      }
    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitEq(op: Opcode, state: ProgramState): SymbolicExecRet = {
    val first = state.stack.pop()
    val second = state.stack.pop()
    if(first.isReal() && second.isReal()){
      val f1 = first.getNumeralValue().get
      val f2 = second.getNumeralValue().get
      if(f1 == f2)
        state.stack.push(IntItem(1))
      else state.stack.push(IntItem(0))
    } else {
      val cond = z3.mkEq(first.toSymbolic(z3).formula, second.toSymbolic(z3).formula)
      val ifExpr = z3.mkITE(cond, ONEBV, ZEROBV).simplify()
      state.stack.push(FormulaItem(ifExpr))
    }
    state.increasePC()
    NextInstruction()
  }

  private def checkStackUnderFlow(state: ProgramState, lessThanSize: Int) = {
    if(state.stack.size() < lessThanSize)
      throw new RuntimeException("Stack underflow")
  }

  override def visitSgt(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    val first = state.stack.pop()
    val second = state.stack.pop()
    if(first.isReal() && second.isReal()){
      val f1 = first.toSigned()
      val f2 = second.toSigned()
      if(f1 > f2)
        state.stack.push(IntItem(1))
      else state.stack.push(IntItem(0))
    } else {
      val cond = z3.mkBVSGT(first.toSymbolic(z3).formula.asInstanceOf[BitVecExpr], second.toSymbolic(z3).formula.asInstanceOf[BitVecExpr])
      val ite = z3.mkITE(cond, ONEBV, ZEROBV)
      state.stack.push(FormulaItem(ite.simplify()))
    }
    NextInstruction()
  }

  override def visitSlt(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    val first = state.stack.pop()
    val second = state.stack.pop()
    if(first.isReal() && second.isReal()){
      val f1 = first.toSigned()
      val f2 = second.toSigned()
      if(f1 < f2)
        state.stack.push(IntItem(1))
      else state.stack.push(IntItem(0))
    } else {
      val cond = z3.mkBVSLT(first.toSymbolic(z3).formula.asInstanceOf[BitVecExpr], second.toSymbolic(z3).formula.asInstanceOf[BitVecExpr])
      val ite = z3.mkITE(cond, ONEBV, ZEROBV)
      state.stack.push(FormulaItem(ite.simplify()))
    }
    NextInstruction()
  }

  override def visitGt(op: Opcode, state: ProgramState): SymbolicExecRet = {
    val first = state.stack.pop()
    val second = state.stack.pop()
    if(first.isReal() && second.isReal()){
      if(first.toUnsigned().compareTo(second.toUnsigned()) > 0)
        state.stack.push(IntItem(1))
      else state.stack.push(IntItem(0))
    } else {
      val ltExpr = z3.mkBVUGT(first.toSymbolic(z3).formula.asInstanceOf[BitVecExpr], second.toSymbolic(z3).formula.asInstanceOf[BitVecExpr])
      val ifExpr = z3.mkITE(ltExpr, z3.mkBV(1, 256), z3.mkBV(0, 256)).simplify()
      state.stack.push(FormulaItem(ifExpr))
    }
    state.increasePC()
    NextInstruction()
  }

  override def visitLt(op: Opcode, state: ProgramState): SymbolicExecRet = {
    val first = state.stack.pop()
    val second = state.stack.pop()
    if(first.isReal() && second.isReal()){
      if(first.toUnsigned().compareTo(second.toUnsigned()) < 0)
        state.stack.push(IntItem(1))
      else state.stack.push(IntItem(0))
    } else {
      val ltExpr = z3.mkBVULT(first.toSymbolic(z3).formula.asInstanceOf[BitVecExpr], second.toSymbolic(z3).formula.asInstanceOf[BitVecExpr])
      val ifExpr = z3.mkITE(ltExpr, z3.mkBV(1, 256), z3.mkBV(0, 256)).simplify()
      state.stack.push(FormulaItem(ifExpr))
    }
    state.increasePC()
    NextInstruction()
  }

  override def visitSignExtEnd(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitExp(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    val base = state.stack.pop()
    val exponent = state.stack.pop()
    // Type conversion is needed when they are mismatched
    if (base.isReal() && exponent.isReal()) {
      val p = math.pow(2, 256)
      val computed = math.pow(base.getNumeralValue().get.doubleValue(), exponent.getNumeralValue().get.doubleValue()) % p
      state.stack.push(IntItem(BigInt(computed.toLong)))
    }
    else {
      // The computed value is unknown, this is because power is not supported in bit-vector theory
      val new_var_name = generator.gen_arbitrary_var()
      val computed = z3.mkBVConst(new_var_name, 256).simplify()
      state.stack.push(FormulaItem(computed))
    }
    NextInstruction()
  }

  override def visitMulMod(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitAddMod(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSMod(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitMod(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitSDIV(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    val first = state.stack.pop()
    val second = state.stack.pop()
    if(first.isReal() && second.isReal()){
      val f1 = first.toSigned()
      val f2 = second.toSigned()
      if(f2 == 0){
        state.stack.push(IntItem(0))
      } else if (f1 == 0 - math.pow(2, 255) && f2 == -1){
        state.stack.push(IntItem(BigInt((0 - math.pow(2, 255).toLong))))
      } else {
        val sign = if(f1 / f2 < 0) -1 else 1
        val sdiv = sign * (math.abs(f1.toLong) / math.abs(f2.toLong))
        state.stack.push(IntItem(sdiv))
      }
    } else {
      val f1 = first.toSymbolic(z3)
      val f2 = second.toSymbolic(z3)
      solver.push()
      val eq0 = z3.mkEq(f2.formula, ZEROBV)
      solver.add(z3.mkNot(eq0))
      if(solver.check() == Status.UNSATISFIABLE){
        state.stack.push(FormulaItem(ZEROBV))
      } else {
        val cond1 = z3.mkEq(f1.formula, z3.mkInt(0-math.pow(2, 255).toLong))
        val cond2 = z3.mkEq(f2.formula, z3.mkBV(-1, 256))
        val cond = z3.mkNot(z3.mkAnd(cond1, cond2))
        solver.push()
        solver.add(cond)
        if(solver.check() == Status.UNSATISFIABLE){
          state.stack.push(IntItem(BigInt((0 - math.pow(2, 255).toLong))))
        } else {
          solver.push()
          val cond = z3.mkBVSDiv(f1.formula.asInstanceOf[BitVecExpr], f2.formula.asInstanceOf[BitVecExpr])
          solver.add(z3.mkBVSLT(cond, ZEROBV))
          val sign = if(solver.check() == Status.UNSATISFIABLE) z3.mkBV(-1, 256) else ONEBV
          def z3_abs(x: BitVecExpr): BitVecExpr = {
            val icond = z3.mkBVSGE(x, ZEROBV)
            val res = z3.mkITE(icond, x, z3.mkBVSub(ZEROBV, x))
            res.asInstanceOf[BitVecExpr]
          }
          val f1abs = z3_abs(f1.formula.asInstanceOf[BitVecExpr])
          val f2abs = z3_abs(f2.formula.asInstanceOf[BitVecExpr])
          val computed = z3.mkBVMul(sign, z3.mkBVSDiv(f1abs, f2abs))
          state.stack.push(FormulaItem(computed.simplify()))
          solver.pop()
        }
        solver.pop()
      }
      solver.pop()
    }
    NextInstruction()
  }

  override def visitDiv(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size()> 1){
      state.increasePC()
      val first = state.stack.pop()
      val second = state.stack.pop()
      if(first.isReal() && second.isReal()){
        if(second.getNumeralValue().get == 0)
          state.stack.push(IntItem(0))
        else {
          val computed = first.toUnsigned() / second.toUnsigned()
          state.stack.push(IntItem(computed))
        }
      } else {
        val firstSymb = first.toSymbolic(z3)
        val secondSymb = second.toSymbolic(z3)
        solver.push()
        solver.add(z3.mkNot(z3.mkEq(secondSymb.formula, ZEROBV)))
        if(solver.check() == Status.UNSATISFIABLE)
          state.stack.push(IntItem(0))
        else {
          val udiv = z3.mkBVUDiv(firstSymb.formula.asInstanceOf[BitVecExpr], secondSymb.formula.asInstanceOf[BitVecExpr]).simplify()
          state.stack.push(FormulaItem(udiv))
        }
        solver.pop()
      }
    } else { // stack size <= 1
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitSub(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 1){
      state.increasePC()
      val first = state.stack.pop()
      val second = state.stack.pop()
      if(first.isReal() && second.isReal()){
        val sub = (first.getNumeralValue().get - second.getNumeralValue().get) % EXP2256
        state.stack.push(IntItem(sub))
      } else {
        val sub = z3.mkBVSub(first.toSymbolic(z3).formula.asInstanceOf[BitVecExpr], second.toSymbolic(z3).formula.asInstanceOf[BitVecExpr])
        state.stack.push(FormulaItem(sub.simplify()))
      }
    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitMul(op: Opcode, state: ProgramState): SymbolicExecRet = {
    checkStackUnderFlow(state, 2)
    state.increasePC()
    var first = state.stack.pop()
    var second = state.stack.pop()
    if(first.isReal())
      first = IntItem(first.toUnsigned()).toSymbolic(z3)
    if(second.isReal())
      second = IntItem(second.toUnsigned()).toSymbolic(z3)
    val mul = z3.mkBVMul(first.asInstanceOf[FormulaItem].formula.asInstanceOf[BitVecExpr], second.asInstanceOf[FormulaItem].formula.asInstanceOf[BitVecExpr])
    state.stack.push(FormulaItem(mul).simplify())
    NextInstruction()
  }

  override def visitAdd(op: Opcode, state: ProgramState): SymbolicExecRet = {
    if(state.stack.size() > 1){
      state.increasePC()
      val first = state.stack.pop()
      val second = state.stack.pop()
      if(first.isReal() && second.isReal()){
        val f1 = first.getNumeralValue().get
        val f2 = second.getNumeralValue().get
        val res = (f1 + f2) % EXP2256
        state.stack.push(IntItem(res))
      } else {
        val f1 = first.toSymbolic(z3)
        val f2 = second.toSymbolic(z3)
        val res = z3.mkBVAdd(f1.formula.asInstanceOf[BitVecExpr], f2.formula.asInstanceOf[BitVecExpr])
        state.stack.push(FormulaItem(res.simplify()))
      }
    } else {
      throw new RuntimeException("Stack underflow")
    }
    NextInstruction()
  }

  override def visitSwap(op: Opcode, state: ProgramState): SymbolicExecRet = {
    val position = op.toString.replace("SWAP","").toInt
    if (state.stack.size() > position) {
      val temp = state.stack.atPos(position)
      state.stack.update(position, state.stack.peek())
      state.stack.update(0, temp)
    } else throw new RuntimeException("Stack underflow")
    state.increasePC()
    NextInstruction()
  }

  override def visitDup(op: Opcode, state: ProgramState): SymbolicExecRet = {
    val position = op.toString.replace("DUP","").toInt - 1
    if (state.stack.size() > position) {
      val duplicate = state.stack.atPos(position)
      state.stack.push(duplicate)
    } else throw new RuntimeException("Stack underflow")
    state.increasePC()
    NextInstruction()
  }

  override def visitLog(op: Opcode, state: ProgramState): SymbolicExecRet = ???

  override def visitStop(op: Opcode, state: ProgramState): SymbolicExecRet = {
    state.increasePC()
    NextInstruction()
  }

}

