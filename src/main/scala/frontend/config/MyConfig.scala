package frontend.config

import java.io.File

import enumeratum._
import frontend.config.INPUT_TYPE.SOLIDITY

sealed abstract class INPUT_TYPE extends EnumEntry {}

object INPUT_TYPE extends Enum[INPUT_TYPE] {
  val values = findValues
  case object SOLIDITY extends INPUT_TYPE
  case object STANDARD_JSON extends INPUT_TYPE
}

case class MyConfig(loopLimit: Int = 10, gasLimit: Int = 1000, depthLimit: Int = 1000,
                    source: File = new File("."), bin: Boolean = false, disasm: Boolean = false,
                    ipt: INPUT_TYPE = SOLIDITY, solc: String = "solc", evm: String = "evm",
                    useGlobalStorage: Boolean = false, genTestCases: Boolean = false, inputState: Boolean = false,
                    libName: String = "", maxCount: Int = -1, verbose: Boolean = false, debug: Boolean = false,
                    mode: String = "", files: Seq[File] = Seq(), keepalive: Boolean = false,
                    jars: Seq[File] = Seq(), kwargs: Map[String,String] = Map()) {

}

object MyConfig {
  lazy val config = argsParsing(args)
  var args: Array[String] = null

  private def argsParsing(args: Array[String]): MyConfig = {
    val parser = new scopt.OptionParser[MyConfig]("scopt") {
      head("scopt", "3.x")

      opt[Int]('l', "looplm").action( (x, c) =>
        c.copy(loopLimit = x) ).text("Loop limit for symbolic execution")

      opt[Int]('g', "glm").action( (x, c) =>
        c.copy(gasLimit = x) ).text("Gas limit for symbolic execution")

      opt[Int]('d', "dlm").action( (x, c) =>
        c.copy(depthLimit = x) ).text("Depth limit for symbolic execution")

      opt[File]('s', "source").required().valueName("<file>").
        action( (x, c) => c.copy(source = x) ).
        text("Source file to run analysis")

      opt[Unit]('b', "bin").action( (_, c) =>
        c.copy(bin = true) ).text("Indicate that input file is binary")

      opt[Unit]('a', "asm").action( (_, c) =>
        c.copy(disasm = true) ).text("Indicate that input file is asm")

      opt[String]("ipt").action( (x, c) =>
        c.copy(ipt = INPUT_TYPE.withName(x.toUpperCase)) ).text("Indicate input type is solidity or standard json")

      opt[String]("solc").action( (x, c) =>
        c.copy(solc = x) ).text("Solc compiler")

      opt[String]("evm").action( (x, c) =>
        c.copy(evm = x) ).text("evm virtual machine")

      opt[Unit]('g', "globstore").action( (_, c) =>
        c.copy(useGlobalStorage = true) ).text("Use global storage")

      opt[Unit]('t', "gentests").action( (_, c) =>
        c.copy(genTestCases = true) ).text("Generate test cases")

      opt[Unit]('i', "inputstate").action( (_, c) =>
        c.copy(inputState = true) ).text("Input State")

      opt[(String, Int)]("max").action({
        case ((k, v), c) => c.copy(libName = k, maxCount = v) }).
        validate( x =>
          if (x._2 > 0) success
          else failure("Value <max> must be >0") ).
        keyValueName("<libname>", "<max>").
        text("maximum count for <libname>")

      opt[Seq[File]]('j', "jars").valueName("<jar1>,<jar2>...").action( (x,c) =>
        c.copy(jars = x) ).text("jars to include")

      opt[Map[String,String]]("kwargs").valueName("k1=v1,k2=v2...").action( (x, c) =>
        c.copy(kwargs = x) ).text("other arguments")

      opt[Unit]("verbose").action( (_, c) =>
        c.copy(verbose = true) ).text("verbose is a flag")

      opt[Unit]("debug").hidden().action( (_, c) =>
        c.copy(debug = true) ).text("this option is hidden in the usage text")

      help("help").text("prints this usage text")

      arg[File]("<file>...").unbounded().optional().action( (x, c) =>
        c.copy(files = c.files :+ x) ).text("optional unbounded args")

      /*cmd("update").action( (_, c) => c.copy(mode = "update") ).
        text("update is a command.").
        children(
          opt[Unit]("not-keepalive").abbr("nk").action( (_, c) =>
            c.copy(keepalive = false) ).text("disable keepalive"),
          opt[Boolean]("xyz").action( (x, c) =>
            c.copy(xyz = x) ).text("xyz is a boolean property"),
          opt[Unit]("debug-update").hidden().action( (_, c) =>
            c.copy(debug = true) ).text("this option is hidden in the usage text"),
          checkConfig( c =>
            if (c.keepalive && c.xyz) failure("xyz cannot keep alive")
            else success )
        )*/
    }

    // parser.parse returns Option[C]
    parser.parse(args, MyConfig()) match {
      case Some(config) => config

      case None => null
      // arguments are bad, error message will have been displayed
    }
  }
}