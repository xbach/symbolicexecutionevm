package frontend.parser

import frontend.config.{INPUT_TYPE, MyConfig}
import play.api.libs.json._
import utils.Utils

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

case class Source(fileName: String){
  lazy val content: String = loadContent()
  lazy val lineBreakPositions: ArrayBuffer[Int] = loadLineBreakPositions()

  private def loadContent() = {
    scala.io.Source.fromFile(fileName).mkString
  }

  private def loadLineBreakPositions(): ArrayBuffer[Int] = {
    var i = 0
    val res = new ArrayBuffer[Int]()
    while (i < content.length){
      if(content.charAt(i) == '\n')
        res += i
      i += 1
    }
    res
  }
}

/**
  * Holds map from contract name to its source content (String)
  * Allows to map from (evm binary) instruction to its position in the source content,
  * subsequently allows to retrieve the corresponding source code for that instruction
  * Note that an instruction is determined by its instruction address. During symbolic
  * execution, an instruction address is known by program counter (pc)
  * @param cname
  * @param parentFilename
  * @param rootPath
  */
case class SourceMap(cname: String, parentFilename: String, rootPath: String){
  val astHelper = new AstHelper(parentFilename)
  lazy val varNames = astHelper.extract_state_variable_names(cname)

  lazy val position_groups: JsValue = {
    if(MyConfig.config.ipt == INPUT_TYPE.SOLIDITY){
      loadPositionGroups()
    } else null
  }

  lazy val instructionPositions = new mutable.HashMap[BigInt, JsValue]()

  lazy val positions: JsArray = getPositions()

  lazy val source = getSource()

  lazy val funcCallNames = _get_func_call_names()

  def init() = {
    source
  }

  init()

  private def loadPositionGroups(): JsValue = {
    val cmd = MyConfig.config.solc + " --combined-json asm " + parentFilename
    val out = Utils.runCmd(cmd)
    val jsonOut = Json.parse(out)
    jsonOut("contracts")
  }

  private def getSource(): Source = {
    val fName = getFilename()
    Parser.getSource(fName) match {
      case Some(source) => source
      case None => Parser.addSource(fName, Source(fName))
    }
  }

  private def getPositions(): JsArray = {
    var asm = if (MyConfig.config.ipt == INPUT_TYPE.SOLIDITY)
      position_groups(cname)("asm")(".data")("0")
    else {
      val lastSemiColon = cname.lastIndexOf(":")
      val sp = cname.splitAt(lastSemiColon) //cname.split(":")
      val filename = sp._1 //sp(0)
      val contract_name = sp._2.replace(":","") //sp(1)
      position_groups(filename)(contract_name)("evm")("legacyAssembly")(".data")("0")
    }

    var break = false
    val res = asm(".code").asInstanceOf[JsArray]
    while(!break){
      try {
        res.append(asm(".data")("0")(".code"))
        asm = asm(".data")("0")
      }
      catch {
        case _: Throwable => break = true
      }
    }

    return res
  }

  def getSourceCode(pc: BigInt): String = {
    val pos = instructionPositions.getOrElse(pc, throw new RuntimeException("Not found mapping for pc: "+pc))
    val begin = Integer.valueOf(pos("begin").toString())
    val end = Integer.valueOf(pos("end").toString())
    source.content.substring(begin, end)
  }

  def _get_func_call_names(): ArrayBuffer[String] = {
    val func_call_srcs = astHelper.extract_func_call_srcs(cname)
    val func_call_names = new ArrayBuffer[String]()
    func_call_srcs.foreach(src => {
      val sp = src.asInstanceOf[JsString].value.split(":")
      val start = sp(0).toInt
      val end = start + sp(1).toInt
      func_call_names.append(source.content.substring(start, end))
    })
    return func_call_names
  }

  def getAST(pc: BigInt): JsValue = {
    val pos = instructionPositions.getOrElse(pc, throw new RuntimeException("Not found mapping for pc: "+pc))
    //println("*********"+Utils.convertOffset2LineColumn(pos.asInstanceOf[JsObject], source))
    val begin = Integer.valueOf(pos("begin").toString())
    val end = Integer.valueOf(pos("end").toString())
    astHelper.getASTByPos(begin, end, cname)
  }

  def getVarName(pc: BigInt): String = {
    val ast = getAST(pc)
    val res = astHelper.getFirstVarName(ast)
    println("****** SLOAD: "+res)
    res
  }

  def getFilename(): String = {
    cname.substring(0, cname.lastIndexOf(":"))
  }

  def isParamOrStateVar(v: String): Boolean = {
    if(v == null)
      return false
    varNames.contains(v)
  }
}
object TestSource{
  def main(args: Array[String]): Unit = {
    val file = "/home/dxble/workspace/symbolicexecutionevm/src/main/scala/testFastParse/add.sol"
    val s = new Source(file)
    println(s.content)
    println(s.lineBreakPositions)
    MyConfig.args = args
    val config = MyConfig.config
    val sm = new SourceMap(file+":Addition", file, "")
    println(sm.positions)
    println(sm.getAST(0))
  }
}