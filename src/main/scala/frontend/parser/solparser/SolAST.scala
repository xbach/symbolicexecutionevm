package frontend.parser.solparser

sealed trait Position{
  def getLine(): Int
  def getColumn(): Int
}
case class LineColPos(line: Int, col: Int) extends Position{
  override def getLine() = line

  override def getColumn() = col
}
case class NoPos() extends Position {
  override def getLine() = ???

  override def getColumn() = ???
}

abstract class SolAST(pos: Position) {
  def getPosition(): Position = pos
}
//case class
