package frontend.parser

import com.typesafe.scalalogging.Logger
import frontend.ast._
import frontend.config.MyConfig
import play.api.libs.json.{JsArray, JsNull, JsString}
import utils.Utils

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

case class Input(contract: String, sourceMap: SourceMap, source: String, cSource: String, cName: String, disasmFile: String)

object Parser{
  val sources = new mutable.HashMap[String, Source]()
  def getSource(filePath: String): Option[Source] = sources.get(filePath)
  def addSource(filePath: String, source: Source): Source = {
    sources += filePath -> source
    source
  }
}

class Parser() {
  //val inputs = new mutable.HashMap[String, ]()

  val logger = Logger(classOf[Parser])

  def evmTemp(target: String) = {
    val name  = replaceLast(target, ":", Utils.delim)
    "%s.evm".format(name)
  }
  def disasmTemp(target: String) = {
    val name  = replaceLast(target, ":", Utils.delim)
    "%s.evm.disasm".format(name)
  }
  def logTemp(target: String) = {
    val name  = replaceLast(target, ":", Utils.delim)
    "%s.evm.disasm.log".format(name)
  }


  def getInputs() = {
    val inputs = new ArrayBuffer[Input]()
    if(MyConfig.config.bin){
      //TODO: handle binary input later
      val bytecode = scala.io.Source.fromFile(MyConfig.config.source).mkString
      prepareDisasmFile(MyConfig.config.source.getAbsolutePath, bytecode)
      inputs.append(Input(null, null, MyConfig.config.source.getAbsolutePath, null, null, disasmTemp(MyConfig.config.source.getAbsolutePath)))

    } else if(MyConfig.config.disasm){
      inputs.append(Input(null, null, MyConfig.config.source.getAbsolutePath, null, null, MyConfig.config.source.getAbsolutePath))
    }
    else {
      val contracts = getCompiledContracts()
      prepareDisasmFilesForAnalysis(contracts)

      // One source file may contain multiple contract definitions
      // We iterate over each contract, and create a source map for each contract
      // A source map is meant to map back from each (binary) instruction to corresponding
      // (original solidity) source code region
      for ((contract, _) <- contracts){
        val lastSemi = contract.lastIndexOf(":")
        val (c_source, cname) = contract.splitAt(lastSemi)
        val source_map = SourceMap(contract, MyConfig.config.source.getAbsolutePath, "")
        val disasm_file = disasmTemp(contract)
        inputs.append(Input(contract, source_map, MyConfig.config.source.getAbsolutePath, c_source, cname.replace(":",""), disasm_file))
      }
    }
    inputs
  }

  private def getCompiledContracts() = {
    compileSolidity(MyConfig.config.source.getAbsolutePath)
  }

  private def prepareDisasmFilesForAnalysis(contracts: ArrayBuffer[(String, String)]): Unit= {
    for ((contract, bytecode) <- contracts)
      prepareDisasmFile(contract, bytecode)
  }


  private def prepareDisasmFile(target: String, bytecode: String): Unit = {
    writeEvmFile(target, bytecode)
    writeDisasmFile(target)
  }

  private def writeEvmFile(target: String, bytecode: String): Unit = {
    val evm_file = evmTemp(target)
    Utils.fileWriter(evm_file, removeSwarmHash(bytecode))
  }

  private def removeSwarmHash(evm: String): String = {
    // evm 606060405260043610603f576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff168063a5f3c23b146044575b600080fd5b3415604e57600080fd5b606b6004808035906020019091908035906020019091905050606d565b005b6002821315608257808201600081905550608a565b816000819055505b50505600a165627a7a723058203754ec31d44f9384598157e73c4b40145d094f564c38628e35e578f3c5abc02f0029
    // res 606060405260043610603f576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff168063a5f3c23b146044575b600080fd5b3415604e57600080fd5b606b6004808035906020019091908035906020019091905050606d565b005b6002821315608257808201600081905550608a565b816000819055505b50505600
    import scala.util.matching.Regex
    val p = new Regex("a165627a7a72305820\\S{64}0029$")
    val evm_without_hash = p.replaceAllIn(evm, "")
    return evm_without_hash
  }

  private def writeDisasmFile(target: String): Unit = {
    val evm_file = evmTemp(target)
    val disasm_file = disasmTemp(target)
    val evm = if(Utils.isWindows) "\"%s\"".format(MyConfig.config.evm) else MyConfig.config.evm
    val cmd = evm + " disasm "+evm_file
    val disasm_out = Utils.runCmd(cmd)
    Utils.fileWriter(disasm_file, disasm_out)
  }


  private def linkLibraries(source: String, libSet: mutable.HashSet[String]): ArrayBuffer[(String, String)] = {
    var option = ""
    libSet.zipWithIndex.foreach( f => {
      val (lib: String, idx: Int) = f
      val idxp = idx+1
      val lib_address = "0x" + idxp.toHexString.format(40)
      option += " --libraries %s:%s".format(lib, lib_address)
    })

    null
    /*FNULL = open(os.devnull, 'w')
    cmd = "solc --bin-runtime %s" % filename
    p1 = subprocess.Popen(shlex.split(cmd), stdout=subprocess.PIPE, stderr=FNULL)
    cmd = "solc --link%s" %option
    p2 = subprocess.Popen(shlex.split(cmd), stdin=p1.stdout, stdout=subprocess.PIPE, stderr=FNULL)
    p1.stdout.close()
    out = p2.communicate()[0].decode()
    return self._extract_bin_str(out)*/
  }

  private def compileSolidity(source: String): ArrayBuffer[(String, String)] = {
    val cmd = MyConfig.config.solc +" --bin-runtime " + source
    val out = Utils.runCmd(cmd)

    import scala.util.matching.Regex

    val pattern = new Regex("_+(.*?)_+")
    val libs = pattern findAllIn out
    val libSet = libs.foldLeft(new mutable.HashSet[String]()){(res, lib) => {
      res += lib
      res
    }}

    if (!libSet.isEmpty)
      return linkLibraries(source, libSet)
    else
      return extractBinStr(out)
  }

  /**
    *
    * @param s
    * @return array of pairs, each pair is file path and binary bytecode of the file
    */
  private def extractBinStr(s: String): ArrayBuffer[(String, String)] = {
    import scala.util.matching.Regex
    val binary_regex = /*if (Utils.isWindows)
      new Regex("\r\n======= (.*?) =======\r\nBinary of the runtime part: \r\n(.*?)\r\n")
    else*/ new Regex("\n======= (.*?) =======\nBinary of the runtime part: \n(.*?)\n")

    val contracts = binary_regex findAllIn s

    val res = new ArrayBuffer[(String, String)]()
    while(contracts.hasNext){
      contracts.next()
      if(contracts.groupCount == 2) {
        val name = contracts.group(1) //replaceLast(contracts.group(1), ":", Utils.delim)
        val bytecode = contracts.group(2)
        res.append((name, bytecode))
      }
    }

    if(res.isEmpty)
      throw new RuntimeException("Solidity compilation failed!")
    else res
  }


  private def replaceLast(string: String, substring: String, replacement: String): String = {
    val index = string.lastIndexOf(substring)
    if (index == -1) return string
    string.substring(0, index) + replacement + string.substring(index + substring.length)
  }

  def parseEvmDisasm(file: String, sourceMap: SourceMap): Program = {
    // First line is the bytecode, so drop it
    val content = scala.io.Source.fromFile(file).getLines().drop(1)
    val insts = new ArrayBuffer[Instruction]()
    var index = 0
    val length = if(sourceMap != null) sourceMap.positions.value.size else 0

    content.foreach(l => {
      val line = l.replace("SELFDESTRUCT", "SUICIDE").replace("Missing opcode 0xfd", "REVERT").replace("Missing opcode 0xfe", "ASSERTFAIL").replace("Missing opcode", "INVALID")
      val sp = line.split(":")
      val addr = sp(0)
      val opcode = sp(1).trim
      try{
        val instr = Instruction0Arg(Opcode.withName(opcode), BigInt(addr))
        insts.append(instr)
        if(sourceMap != null)
          index = mappingNonPushInstruction(instr, sourceMap, sourceMap.positions, index, length)
      }catch {
        case e: java.util.NoSuchElementException => {
          // Handle push commands here
          if(opcode.startsWith("PUSH")) {
            val op = opcode.split(" ")
            val push = op(0).trim
            val arg = op(1)
            val pushOp = Opcode.withName(push)
            //pushOp.asInstanceOf[PUSH].setArg(arg)
            val instr = Instruction1Arg(pushOp, BigInt(addr), arg)
            insts.append(instr)
            if(sourceMap != null)
              index = mappingPushInstruction(instr, sourceMap, sourceMap.positions, index, length)

          }
          else if(opcode.startsWith("INVALID")){
            val instr = Instruction0Arg(Opcode.withName("INVALID"), BigInt(addr))
            insts.append(instr)
            if(sourceMap != null)
              index = mappingNonPushInstruction(instr, sourceMap, sourceMap.positions, index, length)
          }
        }
      }
    })
    InstructionList(insts)
  }

  private def mappingNonPushInstruction(instruction0Arg: Instruction0Arg, sourceMap: SourceMap,
                                        positions: JsArray, index: Int, length: Int): Int = {
    var idx = index

    while (idx < length){
      val pos = positions(idx)
      pos match {
        case JsNull => return idx + 1
        case _ => {
          val name = pos("name").asInstanceOf[JsString]
          if(name.value.startsWith("tag"))
            idx += 1
          else {
            val instrName = instruction0Arg.opcode
            if (name.value.compareTo(instrName.toString) == 0 ||
              name.value.compareTo("INVALID") == 0 && instrName == Opcode.ASSERTFAIL ||
              name.value.compareTo("KECCAK256") == 0 && instrName == Opcode.SHA3 ||
              name.value.compareTo("SELFDESTRUCT") == 0 && instrName == Opcode.SUICIDE){

              sourceMap.instructionPositions += instruction0Arg.addr -> pos
              return idx + 1
            } else {
              throw new RuntimeException("Source map error")
            }

          }
        }
      }
    }

    idx
  }

  private def mappingPushInstruction(instruction1Arg: Instruction1Arg, sourceMap: SourceMap,
                                     positions: JsArray, index: Int, length: Int): Int = {
    var idx = index
    // idx is the index we are traversing in the ast parsed from source code.
    // The ast was provided by using solc compiler with option --combined-json ast
    while (idx < length){
      val pos = positions(idx)
      pos match {
        case JsNull => return idx + 1
        case _ => {
          val name = pos("name").asInstanceOf[JsString]
          // We dont care about tag, so must not be what we are looking for.
          // Thus, increase the index and look forward until we find one matched
          if(name.value.startsWith("tag"))
            idx += 1
          else {
            if (name.value.startsWith("PUSH")){
              if(name.value.compareTo("PUSH") == 0){
                // Sanity check for the value of PUSH
                val value = BigInt(pos("value").asInstanceOf[JsString].value, 16)
                val instrValue = BigInt(instruction1Arg.asInstanceOf[Instruction1Arg].arg.replace("0x",""), 16)
                if (value.compare(instrValue) == 0) {
                  sourceMap.instructionPositions += instruction1Arg.addr -> pos
                  return idx + 1
                } else
                  throw new RuntimeException("Source map error")

              } else {
                sourceMap.instructionPositions += instruction1Arg.addr -> pos
                return idx + 1
              }
            } else {
              throw new RuntimeException("Source map error")
            }
          }
        }
      }
    }

    idx
  }
}

object TestParser {
  def main(args: Array[String]): Unit = {
    //testExtBinStr()
    //testLinkLibs()
    val file = "D:\\workspace\\symexecEVM\\src\\main\\scala\\testFastParse\\add.sol"
    MyConfig.args = args
    val config = MyConfig.config
    val p = new Parser()
    val inp = p.getInputs()
    println(inp)
  }

  def testExtBinStr() = {
    val parser = new Parser()
    val s1 = "\n======= /home/dxble/1 =======\nBinary of the runtime part: \n6061\n"
    val s2 = "\n======= /home/dxble/2 =======\nBinary of the runtime part: \n6062\n"
    val s3 = s1+s2
    //println(parser.extractBinStr(s3))
  }

  def testLinkLibs() = {
    val parser = new Parser()
    val s = new mutable.HashSet[String]()
    s.add("hhh")
    s.add("ppp")
    //parser.linkLibraries("kkkk", s)
  }
}