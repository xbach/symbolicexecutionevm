package frontend.parser
import frontend.config.{INPUT_TYPE, MyConfig}
import play.api.libs.json._
import utils.Utils

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class AstWalker {

  def walkWithPosition(node: JsObject, begin: Integer, end: Integer): JsObject = {
    val srcPos = node("src").asInstanceOf[JsString].value.split(":")
    val sbegin = srcPos(0).toInt
    val send = srcPos(1).toInt + begin
    if(sbegin == begin && send == end)
      return node
    else {
      if(!node.keys.contains("children"))
        return null

      val nameResult: JsResult[JsArray] = (node \ "children").validate[JsArray]

      // Pattern matching
      nameResult match {
        case s: JsSuccess[JsArray] => {
          s.get.value.foreach(child => {
            val res = walkWithPosition(child.asInstanceOf[JsObject], begin, end)
            if(res != null)
              return res
          })
        }
        case e: JsError => return null
      }

      return null
    }
  }

  def walkWithAttrs(node: JsObject, attributes: JsObject, nodes: ArrayBuffer[JsValue]): Unit = {
    if (checkAttributes(node, attributes))
      nodes.append(node)
    else{
      if(!node.keys.contains("children"))
        return

      val nameResult: JsResult[JsArray] = (node \ "children").validate[JsArray]

      // Pattern matching
      nameResult match {
        case s: JsSuccess[JsArray] => {
          s.get.value.foreach(child => {
            walkWithAttrs(child.asInstanceOf[JsObject], attributes, nodes)
          })
        }
        case e: JsError => return
      }
    }
  }

  /**
    *
    * @param node
    * @param attributes can be JsArray or one JsObject alone
    * @param collectedResult
    */
  def walk(node: JsObject, attributes: JsValue, collectedResult: ArrayBuffer[JsValue]) = {
    attributes match {
      case obj: JsObject => walkWithAttrs(node, obj, collectedResult)
      case arr: JsArray => walkWithListOfAttrs(node, arr, collectedResult)
    }
  }

  private def checkListOfAttributes(node: JsObject, list_of_attributes: JsArray): Boolean = {
    for (attrs <- list_of_attributes.value){
      if (checkAttributes(node, attrs.asInstanceOf[JsObject]))
        return true
    }

    return false
  }

  private def walkWithListOfAttrs(node: JsObject, list_of_attributes: JsArray, nodes: ArrayBuffer[JsValue]): Unit = {
    if (checkListOfAttributes(node, list_of_attributes))
      nodes.append(node)
    else {
      if(!node.keys.contains("children"))
        return

      val nameResult: JsResult[JsArray] = (node \ "children").validate[JsArray]

      // Pattern matching
      nameResult match {
        case s: JsSuccess[JsArray] => {
          s.get.value.foreach(child => {
            walkWithListOfAttrs(child.asInstanceOf[JsObject], list_of_attributes, nodes)
          })
        }
        case e: JsError => return
      }
    }

  }

  def checkAttributes(node: JsObject, attributes: JsObject): Boolean = {
    for (name <- attributes.keys){
      if (name.compareTo("attributes") == 0){
        if (!node.keys.contains("attributes") || !checkAttributes(node("attributes").asInstanceOf[JsObject], attributes("attributes").asInstanceOf[JsObject]))
          return false
      } else {
        if (!node.keys.contains(name) || node(name).toString().compareTo(attributes(name).toString()) != 0)
          return false
      }
    }
    true
  }
}

class ContractDefinitions() {
  val contractsById = new mutable.HashMap[BigDecimal, JsValue]()
  val contractsByName = new mutable.HashMap[String, JsValue]()
  val sourcesByContract = new mutable.HashMap[BigDecimal, String]()
}

/**
  * Map from source position to each ast node
  * Can be used to query type of an ast node
  * @param filename
  * @param config
  */
class AstHelper (filename: String){

  def getFirstVarName(ast: JsValue): String = {
    val collectedNodes = new ArrayBuffer[JsValue]()
    val attr = Json.obj("name" -> "Identifier")
    walker.walk(ast.asInstanceOf[JsObject], attr, collectedNodes)
    if(collectedNodes.isEmpty)
      return null
    collectedNodes(0)("attributes")("value").asInstanceOf[JsString].value
  }

  def getASTByPos(begin: Integer, end: Integer, cname: String): JsValue = {
    contracts.contractsByName.get(cname) match {
      case None => throw new RuntimeException("Cannot find contract: "+cname)
      case Some(contract) => {
        val res = walker.walkWithPosition(contract.asInstanceOf[JsObject], begin, end)
        if(res == null){
          contracts.contractsByName.keySet.foreach(name => {
            if(name.compareTo(cname) != 0){
              val ct = contracts.contractsByName.get(name).get.asInstanceOf[JsObject]
              val ast = walker.walkWithPosition(ct, begin, end)
              if(ast != null)
                return ast
            }
          })
          return null
        } else return res
      }
    }

  }


  lazy val SourceList = {
    if (MyConfig.config.ipt == INPUT_TYPE.SOLIDITY)
      getSourceList()
    else getSourceListStandardJson()
  }

  lazy val walker = new AstWalker()

  lazy val contracts = extractContractDefinitions()

  private def extract_func_call_definitions(c_name: String): ArrayBuffer[JsValue] = {
    val node = contracts.contractsByName.get(c_name)
    val nodes = new ArrayBuffer[JsValue]
    if(node != None)
      walker.walk(node.get.asInstanceOf[JsObject], Json.obj("name" -> "FunctionCall"), nodes)
    return nodes
  }

  private def extract_func_calls_definitions(): mutable.HashMap[String, ArrayBuffer[JsValue]] = {
    val ret = new mutable.HashMap[String, ArrayBuffer[JsValue]]()
    contracts.contractsById.foreach{
      contract => {
        val name = contracts.contractsById.get(contract._1).get("attributes")("name").asInstanceOf[JsString].value
        val source = contracts.sourcesByContract.get(contract._1).get
        val full_name = source + ":" + name
        val defs = extract_func_call_definitions(full_name)
        ret.update(full_name, defs)
      }
    }
    return ret
  }

  def extract_func_call_srcs(c_name: String): ArrayBuffer[JsValue] = {
    val func_calls = extract_func_calls_definitions().get(c_name).get
    val func_call_srcs = new ArrayBuffer[JsValue]()
    func_calls.foreach(func_call => {
      func_call_srcs.append(func_call("src"))
    })
    return func_call_srcs
  }

  private def extractContractDefinitions(): ContractDefinitions = {
    val contractDef = new ContractDefinitions
    for(s <- SourceList.keys){
      var ast: JsObject = null
      if(MyConfig.config.ipt == INPUT_TYPE.SOLIDITY)
        ast = SourceList(s)("AST").asInstanceOf[JsObject]
      else ast = SourceList(s)("legacyAST").asInstanceOf[JsObject]

      val collectedNodes = new ArrayBuffer[JsValue]()
      val attr = Json.obj("name" -> "ContractDefinition")
      walker.walk(ast, attr, collectedNodes)
      collectedNodes.foreach(node => {
        val id = node("id").asInstanceOf[JsNumber].value
        contractDef.contractsById +=  id -> node
        contractDef.sourcesByContract += id -> s
        contractDef.contractsByName += s + ":" + node("attributes")("name").asInstanceOf[JsString].value -> node
      })
    }
    contractDef
  }

  private def getSourceListStandardJson(): JsObject = {
    val out = Json.parse("standard_json_output")
    out("sources").asInstanceOf[JsObject]
  }

  private def getSourceList(): JsObject = {
    val cmd = MyConfig.config.solc + " --combined-json ast " + filename
    val out = Utils.runCmd(cmd)
    val jsonOut = Json.parse(out)
    jsonOut("sources").asInstanceOf[JsObject]
  }
  
  def get_linearized_base_contracts(value: JsValue) = {
    val id = BigDecimal(value.toString())
    val res = contracts.contractsById(id)("attributes")("linearizedBaseContracts").asInstanceOf[JsArray].value.map( x =>
      contracts.contractsById(BigDecimal(x.toString())))
    res
  }

  def extract_state_definitions(cname: String): ArrayBuffer[JsValue] = {
    val node: JsValue = contracts.contractsByName(cname)
    val state_vars = new ArrayBuffer[JsValue]()
    node match {
      case JsNull =>
      case _ => {
        val base_contracts = get_linearized_base_contracts(node("id")).reverse
        base_contracts.foreach(contract => {
          contract.\("children").validate[JsArray] match {
            case arr: JsSuccess[JsArray] => {
              for (item <- arr.get.value)
                if (item("name").asInstanceOf[JsString].value.compareTo("VariableDeclaration") == 0)
                  state_vars.append(item)
            }
            case _: JsError =>
          }
        })
      }
    }
    return state_vars
  }

  def extract_states_definitions(): mutable.HashMap[String, ArrayBuffer[JsValue]] = {
    val ret = new mutable.HashMap[String, ArrayBuffer[JsValue]]()
    for (contract <- contracts.contractsById.keySet){
      val name = contracts.contractsById.get(contract).get("attributes")("name").asInstanceOf[JsString].value
      val source = contracts.sourcesByContract.get(contract).get
      val full_name = source + ":" + name
      ret.getOrElseUpdate(full_name, extract_state_definitions(full_name))
    }

    return ret
  }

  def extract_state_variable_names(cname: String): ArrayBuffer[String] = {
    val state_variables = extract_states_definitions().get(cname).get
    val var_names = new ArrayBuffer[String]()
    for (var_name <- state_variables)
      var_names.append(var_name("attributes")("name").asInstanceOf[JsString].value)
    return var_names
  }

}

object TestAst {
  def main(args: Array[String]): Unit = {
    val file = "/home/dxble/workspace/symbolicexecutionevm/src/main/scala/testFastParse/add.sol"
    //val file = "D:\\workspace\\symexecEVM\\src\\main\\scala\\testFastParse\\add.sol"
    MyConfig.args = args
    val config = MyConfig.config
    val astHelper = new AstHelper(file)
    println(astHelper.SourceList)
    val walker = new AstWalker
    val res = new ArrayBuffer[JsValue]()
    astHelper.SourceList.value.foreach{ case (k, node) => {
      val ast = node("AST")
      val attr = Json.obj("name" -> "ContractDefinition")
      walker.walk(ast.asInstanceOf[JsObject], attr, res)
    }}
    res.foreach(cd => println("===========" + cd))
    //walker._walk_with_attrs()
  }
}
