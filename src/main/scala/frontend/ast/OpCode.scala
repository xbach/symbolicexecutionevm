package frontend.ast

import enumeratum._
import backend.symexec.ProgramState

sealed abstract class Opcode(val order: Int) extends EnumEntry with Program {
  override def toOpCodeString(): String = this.toString
}
sealed trait PUSH extends Opcode with WVerylow {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitPush, this, state)
}
sealed trait DUP extends Opcode with WVerylow {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitDup, this, state)
}
sealed trait SWAP extends Opcode with WVerylow {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSwap, this, state)
}
sealed trait LOG extends Opcode {
  def numTopics: Int
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitLog, this, state)
}

sealed trait WClassification {
}
sealed trait WZero extends WClassification {
}
sealed trait WBase extends WClassification {
}
sealed trait WVerylow extends WClassification {
}
sealed trait WLow extends WClassification {
}
sealed trait WMid extends WClassification {
}
sealed trait WHigh extends WClassification {
}
sealed trait WExt extends WClassification {
}

case class StackRW(read: Int, write: Int)

object Opcode extends Enum[Opcode] {
  val values = findValues

  case object STOP extends Opcode(0x00) with WZero {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitStop, this, state)
  }
  case object ADD extends Opcode(0x01) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitAdd, this, state)
  }
  case object MUL extends Opcode(0x02) with WLow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMul, this, state)
  }
  case object SUB extends Opcode(0x03)  with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSub, this, state)
  }
  case object DIV extends Opcode(0x04) with WLow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitDiv, this, state)
  }
  case object SDIV extends Opcode(0x05) with WLow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSDIV, this, state)
  }
  case object MOD extends Opcode(0x06) with WLow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMod, this, state)
  }
  case object SMOD extends Opcode(0x07) with WLow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSMod, this, state)
  }
  case object ADDMOD extends Opcode(0x08) with WMid {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitAddMod, this, state)
  }
  case object MULMOD extends Opcode(0x09)with WMid {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMulMod, this, state)
  }
  case object EXP extends Opcode(0x0a) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitExp, this, state)
  }
  case object SIGNEXTEND extends Opcode(0x0b) with WLow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSignExtEnd, this, state)
  }

  case object LT extends Opcode(0x10) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitLt, this, state)
  }
  case object GT extends Opcode(0x11) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitGt, this, state)
  }
  case object SLT extends Opcode(0x12) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSlt, this, state)
  }
  case object SGT extends Opcode(0x13) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSgt, this, state)
  }
  case object EQ extends Opcode(0x14) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitEq, this, state)
  }
  case object ISZERO extends Opcode(0x15) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitIsZero, this, state)
  }
  case object AND extends Opcode(0x16) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitAnd, this, state)
  }
  case object OR extends Opcode(0x17) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitOr, this, state)
  }
  case object XOR extends Opcode(0x18) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitXor, this, state)
  }
  case object NOT extends Opcode(0x19) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitNot, this, state)
  }
  case object BYTE extends Opcode(0x1a) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitByte, this, state)
  }
  case object SHA3 extends Opcode(0x20) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSha3, this, state)
  }

  case object ADDRESS extends Opcode(0x30) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitAddress, this, state)
  }
  case object BALANCE extends Opcode(0x31) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitBalance, this, state)
  }
  case object ORIGIN extends Opcode(0x32) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitOrigin, this, state)
  }
  case object CALLER extends Opcode(0x33) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCaller, this, state)
  }
  case object CALLVALUE extends Opcode(0x34) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCallValue, this, state)
  }
  case object CALLDATALOAD extends Opcode(0x35) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCallDataLoad, this, state)
  }
  case object CALLDATASIZE extends Opcode(0x36) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCallDataSize, this, state)
  }
  case object CALLDATACOPY extends Opcode(0x37) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCallDataCopy, this, state)
  }
  case object CODESIZE extends Opcode(0x38) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCodeSize, this, state)
  }
  case object CODECOPY extends Opcode(0x39) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCodeCopy, this, state)
  }
  case object GASPRICE extends Opcode(0x3a) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitGasPrice, this, state)
  }
  case object EXTCODESIZE extends Opcode(0x3b) with WExt {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitExtCodeSize, this, state)
  }
  case object EXTCODECOPY extends Opcode(0x3c) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitExtCodeCopy, this, state)
  }
  case object MCOPY extends Opcode(0x3d) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMCopy, this, state)
  }

  case object BLOCKHASH extends Opcode(0x40) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitBlockHash, this, state)
  }
  case object COINBASE extends Opcode(0x41) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCoinBase, this, state)
  }
  case object TIMESTAMP extends Opcode(0x42) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitTimeStamp, this, state)
  }
  case object NUMBER extends Opcode(0x43) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitNumber, this, state)
  }
  case object DIFFICULTY extends Opcode(0x44) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitDifficulty, this, state)
  }
  case object GASLIMIT extends Opcode(0x45) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitGasLimit, this, state)
  }

  case object POP extends Opcode(0x50) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitPop, this, state)
  }
  case object MLOAD extends Opcode(0x51) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMLoad, this, state)
  }
  case object MSTORE extends Opcode(0x52) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMStore, this, state)
  }
  case object MSTORE8 extends Opcode(0x53) with WVerylow {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMStore8, this, state)
  }
  case object SLOAD extends Opcode(0x54) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSLoad, this, state)
  }
  case object SSTORE extends Opcode(0x55) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSStore, this, state)
  }
  case object JUMP extends Opcode(0x56) with WMid {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitJump, this, state)
  }
  case object JUMPI extends Opcode(0x57) with WHigh {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitJumpi, this, state)
  }
  case object PC extends Opcode(0x58) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitPC, this, state)
  }
  case object MSIZE extends Opcode(0x59) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitMSize, this, state)
  }
  case object GAS extends Opcode(0x5a) with WBase {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitGas, this, state)
  }
  case object JUMPDEST extends Opcode(0x5b) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitJumDest, this, state)
  }
  case object SLOADEXT extends Opcode(0x5c) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSLoadExt, this, state)
  }
  case object SSTOREEXT extends Opcode(0x5d) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSStoreExt, this, state)
  }
  case object SLOADBYTESEXT extends Opcode(0x5c) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSLoadBytesExt, this, state)
  }
  case object SSTOREBYTESEXT extends Opcode(0x5d) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSStoreBytesExt, this, state)
  }

  case object LOG0 extends Opcode(0xa0) with LOG {val numTopics = 0}
  case object LOG1 extends Opcode(0xa1) with LOG {val numTopics = 1}
  case object LOG2 extends Opcode(0xa2) with LOG {val numTopics = 2}
  case object LOG3 extends Opcode(0xa3) with LOG {val numTopics = 3}
  case object LOG4 extends Opcode(0xa4) with LOG {val numTopics = 4}

  case object CREATE extends Opcode(0xf0) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCreate, this, state)
  }
  case object CALL extends Opcode(0xf1) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCall, this, state)
  }
  case object CALLCODE extends Opcode(0xf2) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCallCode, this, state)
  }
  case object RETURN extends Opcode(0xf3) with WZero {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitReturn, this, state)
  }
  case object REVERT extends Opcode(0xfd)  with WZero {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitRevert, this, state)
  }
  case object ASSERTFAIL extends Opcode(0xfe) with WZero {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitAssertFail, this, state)
  }
  case object DELEGATECALL extends Opcode(0xf4) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitDelegateCall, this, state)
  }
  case object BREAKPOINT extends Opcode(0xf5) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitBreakpoint, this, state)
  }
  case object RNGSEED extends Opcode(0xf6) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitRngSeed, this, state)
  }
  case object SSIZEEXT extends Opcode(0xf7) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSSizeExt, this, state)
  }
  case object SLOADBYTES extends Opcode(0xf8) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSLoadBytes, this, state)
  }
  case object SSTOREBYTES extends Opcode(0xf9) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSStoreBytes, this, state)
  }
  case object SSIZE extends Opcode(0xfa) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSSize, this, state)
  }
  case object STATEROOT extends Opcode(0xfb) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitStateRoot, this, state)
  }
  case object TXEXECGAS extends Opcode(0xfc) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitTxexecGas, this, state)
  }
  case object CALLSTATIC extends Opcode(0xfd) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitCallStatic, this, state)
  }
  case object INVALID extends Opcode(0xfe) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitInvalid, this, state)
  }  // Not an opcode use to cause an exception
  case object SUICIDE extends Opcode(0xff) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitSuicide, this, state)
  }
  case object END extends Opcode(0x00) {
    override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = doAccept(visitor, visitor.visitEnd, this, state)
  }

  case object PUSH1 extends Opcode(0x60) with PUSH
  case object PUSH2 extends Opcode(0x61) with PUSH
  case object PUSH3 extends Opcode(0x62) with PUSH
  case object PUSH4 extends Opcode(0x63) with PUSH
  case object PUSH5 extends Opcode(0x64) with PUSH
  case object PUSH6 extends Opcode(0x65) with PUSH
  case object PUSH7 extends Opcode(0x66) with PUSH
  case object PUSH8 extends Opcode(0x67) with PUSH
  case object PUSH9 extends Opcode(0x68) with PUSH
  case object PUSH10 extends Opcode(0x69) with PUSH
  case object PUSH11 extends Opcode(0x69+1) with PUSH
  case object PUSH12 extends Opcode(0x69+2) with PUSH
  case object PUSH13 extends Opcode(0x69+3) with PUSH
  case object PUSH14 extends Opcode(0x69+4) with PUSH
  case object PUSH15 extends Opcode(0x69+5) with PUSH
  case object PUSH16 extends Opcode(0x69+6) with PUSH
  case object PUSH17 extends Opcode(0x69+7) with PUSH
  case object PUSH18 extends Opcode(0x69+8) with PUSH
  case object PUSH19 extends Opcode(0x69+9) with PUSH
  case object PUSH20 extends Opcode(0x69+10) with PUSH
  case object PUSH21 extends Opcode(0x69+11) with PUSH
  case object PUSH22 extends Opcode(0x69+12) with PUSH
  case object PUSH23 extends Opcode(0x69+13) with PUSH
  case object PUSH24 extends Opcode(0x69+14) with PUSH
  case object PUSH25 extends Opcode(0x69+15) with PUSH
  case object PUSH26 extends Opcode(0x69+16) with PUSH
  case object PUSH27 extends Opcode(0x69+17) with PUSH
  case object PUSH28 extends Opcode(0x69+18) with PUSH
  case object PUSH29 extends Opcode(0x69+19) with PUSH
  case object PUSH30 extends Opcode(0x69+20) with PUSH
  case object PUSH31 extends Opcode(0x69+21) with PUSH
  case object PUSH32 extends Opcode(0x69+22) with PUSH

  case object DUP1 extends Opcode(0x80) with DUP
  case object DUP2 extends Opcode(0x81) with DUP
  case object DUP3 extends Opcode(0x82) with DUP
  case object DUP4 extends Opcode(0x83) with DUP
  case object DUP5 extends Opcode(0x84) with DUP
  case object DUP6 extends Opcode(0x85) with DUP
  case object DUP7 extends Opcode(0x86) with DUP
  case object DUP8 extends Opcode(0x87) with DUP
  case object DUP9 extends Opcode(0x88) with DUP
  case object DUP10 extends Opcode(0x89) with DUP
  case object DUP11 extends Opcode(0x89+1) with DUP
  case object DUP12 extends Opcode(0x89+2) with DUP
  case object DUP13 extends Opcode(0x89+3) with DUP
  case object DUP14 extends Opcode(0x89+4) with DUP
  case object DUP15 extends Opcode(0x89+5) with DUP
  case object DUP16 extends Opcode(0x89+6) with DUP

  case object SWAP1 extends Opcode(0x90) with SWAP
  case object SWAP2 extends Opcode(0x91) with SWAP
  case object SWAP3 extends Opcode(0x92) with SWAP
  case object SWAP4 extends Opcode(0x93) with SWAP
  case object SWAP5 extends Opcode(0x94) with SWAP
  case object SWAP6 extends Opcode(0x95) with SWAP
  case object SWAP7 extends Opcode(0x96) with SWAP
  case object SWAP8 extends Opcode(0x97) with SWAP
  case object SWAP9 extends Opcode(0x98) with SWAP
  case object SWAP10 extends Opcode(0x99) with SWAP
  case object SWAP11 extends Opcode(0x99+1) with SWAP
  case object SWAP12 extends Opcode(0x99+2) with SWAP
  case object SWAP13 extends Opcode(0x99+3) with SWAP
  case object SWAP14 extends Opcode(0x99+4) with SWAP
  case object SWAP15 extends Opcode(0x99+5) with SWAP
  case object SWAP16 extends Opcode(0x99+6) with SWAP

}

sealed abstract class GCost(val cost: Int) extends EnumEntry {
}
object GCost extends Enum[GCost] {
  val values = findValues

  case object Gzero extends GCost(0)
  case object Gbase extends GCost(2)
  case object Gverylow extends GCost(3)
  case object Glow extends GCost(5)
  case object Gmid extends GCost(8)
  case object Ghigh extends GCost(10)
  case object Gextcode extends GCost(20)
  case object Gbalance extends GCost(400)
  case object Gsload extends GCost(50)
  case object Gjumpdest extends GCost(1)
  case object Gsset extends GCost(20000)
  case object Gsreset extends GCost(5000)
  case object Rsclear extends GCost(15000)
  case object Rsuicide extends GCost(24000)
  case object Gsuicide extends GCost(5000)
  case object Gcreate extends GCost(32000)
  case object Gcodedeposit extends GCost(200)
  case object Gcall extends GCost(40)
  case object Gcallvalue extends GCost(9000)
  case object Gcallstipend extends GCost(2300)
  case object Gnewaccount extends GCost(25000)
  case object Gexp extends GCost(10)
  case object Gexpbyte extends GCost(10)
  case object Gmemory extends GCost(3)
  case object Gtxcreate extends GCost(32000)
  case object Gtxdatazero extends GCost(4)
  case object Gtxdatanonzero extends GCost(68)
  case object Gtransaction extends GCost(21000)
  case object Glog extends GCost(375)
  case object Glogdata extends GCost(8)
  case object Glogtopic extends GCost(375)
  case object Gsha3 extends GCost(30)
  case object Gsha3word extends GCost(6)
  case object Gcopy extends GCost(3)
  case object Gblockhash extends GCost(20)
}