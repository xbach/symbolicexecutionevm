package frontend.ast

import frontend.ast.GCost._
import frontend.ast.Opcode._

object OpCodeUtils {
  def main(args: Array[String]): Unit = {
    val p = PUSH1
    p match {
      case _: PUSH => println("Matched ")
      case _ => println("Not Matched ")
    }
  }

  lazy val stackRW = {
    Opcode.values.foldLeft(new collection.immutable.HashMap[Opcode, StackRW]())((res, op) => {
      op match {
        case log: LOG => res.+(op -> StackRW(log.numTopics+2, 0))
        case _: PUSH => res.+(op -> StackRW(0,1))
        case _: DUP => res.+(op -> StackRW(op.order - 0x80 + 1, op.order - 0x80 + 2))
        case _: SWAP => res.+(op -> StackRW(op.order - 0x90 + 2, op.order - 0x90 + 2))
        case STOP|JUMPDEST|ASSERTFAIL|BREAKPOINT|INVALID|END => res.+(op -> StackRW(0,0))
        case ADDRESS|ORIGIN|CALLER|CALLVALUE|CALLDATASIZE|CODESIZE|GASPRICE|COINBASE|TIMESTAMP|NUMBER|DIFFICULTY|GASLIMIT|PC|MSIZE|GAS|TXEXECGAS => {
          res.+(op -> StackRW(0,1))
        }
        case ISZERO|NOT|BALANCE|CALLDATALOAD|EXTCODESIZE|BLOCKHASH|MLOAD|SLOAD|RNGSEED|SSIZE|STATEROOT => res.+(op -> StackRW(1,1))
        case POP|JUMP|SUICIDE => res.+(op -> StackRW(1,0))
        case MSTORE|MSTORE8|SSTORE|JUMPI|RETURN|REVERT => res.+(op -> StackRW(2,0))
        case CALLDATACOPY|CODECOPY|MCOPY|SSTOREEXT|SLOADBYTES|SSTOREBYTES => res.+(op -> StackRW(3,0))
        case ADDMOD|MULMOD|CREATE => res.+(op -> StackRW(3,1))
        case SLOADBYTESEXT|SSTOREBYTESEXT => res.+(op -> StackRW(4,0))
        case DELEGATECALL => res.+(op -> StackRW(6,1))
        case CALL|CALLCODE|CALLSTATIC => res.+(op -> StackRW(7,1))
        case _ => res.+(op -> StackRW(2,1))
      }
    })
  }

  def getInsCost(opcode: Opcode): Int = {
    opcode match {
      case _: WZero => Gzero.cost
      case _: WBase => Gbase.cost
      case _: WVerylow => Gverylow.cost
      case _: WLow => Glow.cost
      case _: WMid => Gmid.cost
      case _: WHigh => Ghigh.cost
      case _: WExt => Gextcode.cost
      case EXP => Gexp.cost
      case SLOAD => Gsload.cost
      case JUMPDEST => Gjumpdest.cost
      case SHA3 => Gsha3.cost
      case CREATE => Gcreate.cost
      case CALL => Gcall.cost
      case CALLCODE => Gcall.cost
      case log: LOG => Glog.cost + log.numTopics * Glogtopic.cost
      case EXTCODECOPY => Gextcode.cost
      case CALLDATACOPY => Gverylow.cost
      case CODECOPY => Gverylow.cost
      case BALANCE => Gbalance.cost
      case BLOCKHASH => Gblockhash.cost
    }
  }

  def getStackRW(opcode: Opcode): StackRW = stackRW.getOrElse(opcode, throw new RuntimeException("Not Found OpCode ..."))
}