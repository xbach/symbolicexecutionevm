package frontend.ast


import backend.symexec.ProgramState

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

trait Program {

  protected def doAccept[T <: Program](visitorBase: VisitorBase, visit: (T, ProgramState) => VisitorRet, v: T, state: ProgramState): VisitorRet ={
    val (prevRes, toVisit) = visitorBase.preVisit(v, state)
    if(toVisit) {
      val res = visit(v, state)
      visitorBase.postVisit(v, state)
      res
    } else prevRes
  }

  def accept(visitor: VisitorBase, state: ProgramState): VisitorRet

  def toOpCodeString(): String
}

trait Instruction extends Program {
  def getOpCode(): Opcode

  def getAddr(): BigInt
}

case class Instruction0Arg(opcode: Opcode, addr: BigInt) extends Instruction {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = {
    doAccept(visitor, visitor.visitInstruction, this, state)
  }

  override def toOpCodeString(): String = addr +": " +opcode.toOpCodeString()

  override def getOpCode(): Opcode = opcode

  override def getAddr(): BigInt = addr
}

case class Instruction1Arg(opcode: Opcode, addr: BigInt, arg: String) extends Instruction {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = {
    doAccept(visitor, visitor.visitInstruction, this, state)
  }

  override def toOpCodeString(): String = addr +": " +opcode.toOpCodeString() + " "+arg

  override def getOpCode(): Opcode = opcode

  override def getAddr(): BigInt = addr
}

case class InstructionList(insts: ArrayBuffer[Instruction]) extends Program {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = {
    doAccept(visitor, visitor.visitInstructionList, this, state)
  }

  override def toOpCodeString(): String = insts.foldLeft(""){(res, instr) => res+instr.toOpCodeString()+"\n"}
}
abstract class BasicBlock(instructions: InstructionList) extends Program {
  def getInstructionList() = instructions
}

case class TerminalBlock(instructionList: InstructionList, startAdrr: BigInt) extends BasicBlock(instructionList) {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = {
    doAccept(visitor, visitor.visitTerminalBlock, this, state)
  }

  override def toOpCodeString(): String = "=====Terminal Block Start=====\n" + instructionList.toOpCodeString()
}

case class ConditionalBlock(instructionList: InstructionList, startAdrr: BigInt) extends BasicBlock(instructionList) {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = {
    doAccept(visitor, visitor.visitConditionalBlock, this, state)
  }

  override def toOpCodeString(): String = "=====Conditional Block Start=====\n" + instructionList.toOpCodeString()
}

case class UnconditionalBlock(instructionList: InstructionList, startAdrr: BigInt) extends BasicBlock(instructionList) {
  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = {
    doAccept(visitor, visitor.visitUnconditionalBlock, this, state)
  }

  override def toOpCodeString(): String = "=====Unconditional Block Start=====\n" + instructionList.toOpCodeString()
}

case class BasicBlocksList(basicBlocks: scala.collection.mutable.HashMap[BigInt, BasicBlock],
                           jumDests: ArrayBuffer[BigInt], staticEdges: mutable.HashMap[BigInt, BigInt]) extends Program {

  lazy val blSortedAddr = sortedAddress()

  override def accept(visitor: VisitorBase, state: ProgramState): VisitorRet = {
    doAccept(visitor, visitor.visitBasicBlocks, this, state)
  }

  override def toOpCodeString(): String = {
    blSortedAddr.foldLeft(""){
      (res, bb) => res + bb._2.toOpCodeString() + "\n"
    }
  }

  private def sortedAddress() = basicBlocks.toSeq.sortWith(_._1 < _._1)
}

abstract class VisitorRet(){}
case class EmptyRet() extends VisitorRet

abstract class VisitorBase(){
  def visitEnd(op: Opcode, state: ProgramState): VisitorRet
  def visitSuicide(op: Opcode, state: ProgramState): VisitorRet
  def visitInvalid(op: Opcode, state: ProgramState): VisitorRet
  def visitCallStatic(op: Opcode, state: ProgramState): VisitorRet
  def visitTxexecGas(op: Opcode, state: ProgramState): VisitorRet
  def visitStateRoot(op: Opcode, state: ProgramState): VisitorRet
  def visitSSize(op: Opcode, state: ProgramState): VisitorRet
  def visitSStoreBytes(op: Opcode, state: ProgramState): VisitorRet
  def visitSLoadBytes(op: Opcode, state: ProgramState): VisitorRet
  def visitSSizeExt(op: Opcode, state: ProgramState): VisitorRet
  def visitRngSeed(op: Opcode, state: ProgramState): VisitorRet
  def visitBreakpoint(op: Opcode, state: ProgramState): VisitorRet
  def visitDelegateCall(op: Opcode, state: ProgramState): VisitorRet
  def visitAssertFail(op: Opcode, state: ProgramState): VisitorRet
  def visitRevert(op: Opcode, state: ProgramState): VisitorRet
  def visitReturn(op: Opcode, state: ProgramState): VisitorRet
  def visitCallCode(op: Opcode, state: ProgramState): VisitorRet
  def visitCall(op: Opcode, state: ProgramState): VisitorRet
  def visitCreate(op: Opcode, state: ProgramState): VisitorRet
  def visitSStoreBytesExt(op: Opcode, state: ProgramState): VisitorRet
  def visitSLoadBytesExt(op: Opcode, state: ProgramState): VisitorRet
  def visitSStoreExt(op: Opcode, state: ProgramState): VisitorRet
  def visitSLoadExt(op: Opcode, state: ProgramState): VisitorRet
  def visitJumDest(op: Opcode, state: ProgramState): VisitorRet
  def visitGas(op: Opcode, state: ProgramState): VisitorRet
  def visitMSize(op: Opcode, state: ProgramState): VisitorRet
  def visitPC(op: Opcode, state: ProgramState): VisitorRet
  def visitJumpi(op: Opcode, state: ProgramState): VisitorRet
  def visitJump(op: Opcode, state: ProgramState): VisitorRet
  def visitSStore(op: Opcode, state: ProgramState): VisitorRet
  def visitSLoad(op: Opcode, state: ProgramState): VisitorRet
  def visitMStore8(op: Opcode, state: ProgramState): VisitorRet
  def visitMStore(op: Opcode, state: ProgramState): VisitorRet
  def visitMLoad(op: Opcode, state: ProgramState): VisitorRet
  def visitPop(op: Opcode, state: ProgramState): VisitorRet
  def visitGasLimit(op: Opcode, state: ProgramState): VisitorRet
  def visitDifficulty(op: Opcode, state: ProgramState): VisitorRet
  def visitNumber(op: Opcode, state: ProgramState): VisitorRet
  def visitTimeStamp(op: Opcode, state: ProgramState): VisitorRet
  def visitCoinBase(op: Opcode, state: ProgramState): VisitorRet
  def visitBlockHash(op: Opcode, state: ProgramState): VisitorRet
  def visitMCopy(op: Opcode, state: ProgramState): VisitorRet
  def visitExtCodeCopy(op: Opcode, state: ProgramState): VisitorRet
  def visitExtCodeSize(op: Opcode, state: ProgramState): VisitorRet
  def visitGasPrice(op: Opcode, state: ProgramState): VisitorRet
  def visitCodeCopy(op: Opcode, state: ProgramState): VisitorRet
  def visitCodeSize(op: Opcode, state: ProgramState): VisitorRet
  def visitCallDataCopy(op: Opcode, state: ProgramState): VisitorRet
  def visitCallDataSize(op: Opcode, state: ProgramState): VisitorRet
  def visitCallDataLoad(op: Opcode, state: ProgramState): VisitorRet
  def visitCallValue(op: Opcode, state: ProgramState): VisitorRet
  def visitCaller(op: Opcode, state: ProgramState): VisitorRet
  def visitOrigin(op: Opcode, state: ProgramState): VisitorRet
  def visitBalance(op: Opcode, state: ProgramState): VisitorRet
  def visitAddress(op: Opcode, state: ProgramState): VisitorRet
  def visitSha3(op: Opcode, state: ProgramState): VisitorRet
  def visitByte(op: Opcode, state: ProgramState): VisitorRet
  def visitNot(op: Opcode, state: ProgramState): VisitorRet
  def visitXor(op: Opcode, state: ProgramState): VisitorRet
  def visitOr(op: Opcode, state: ProgramState): VisitorRet
  def visitAnd(op: Opcode, state: ProgramState): VisitorRet
  def visitIsZero(op: Opcode, state: ProgramState): VisitorRet
  def visitEq(op: Opcode, state: ProgramState): VisitorRet
  def visitSgt(op: Opcode, state: ProgramState): VisitorRet
  def visitSlt(op: Opcode, state: ProgramState): VisitorRet
  def visitGt(op: Opcode, state: ProgramState): VisitorRet
  def visitLt(op: Opcode, state: ProgramState): VisitorRet
  def visitSignExtEnd(op: Opcode, state: ProgramState): VisitorRet
  def visitExp(op: Opcode, state: ProgramState): VisitorRet
  def visitMulMod(op: Opcode, state: ProgramState): VisitorRet
  def visitAddMod(op: Opcode, state: ProgramState): VisitorRet
  def visitSMod(op: Opcode, state: ProgramState): VisitorRet
  def visitMod(op: Opcode, state: ProgramState): VisitorRet
  def visitSDIV(op: Opcode, state: ProgramState): VisitorRet
  def visitDiv(op: Opcode, state: ProgramState): VisitorRet
  def visitSub(op: Opcode, state: ProgramState): VisitorRet
  def visitMul(op: Opcode, state: ProgramState): VisitorRet
  def visitAdd(op: Opcode, state: ProgramState): VisitorRet
  def visitSwap(op: Opcode, state: ProgramState): VisitorRet
  def visitDup(op: Opcode, state: ProgramState): VisitorRet
  def visitLog(op: Opcode, state: ProgramState): VisitorRet
  def visitPush(op: Opcode, state: ProgramState): VisitorRet
  def visitStop(op: Opcode, state: ProgramState): VisitorRet
  def preVisit(instruction: Program, state: ProgramState): (VisitorRet, Boolean) = (EmptyRet(), true)

  def postVisit(instruction: Program, state: ProgramState): Unit = {}

  def visitInstruction(instruction: Instruction, state: ProgramState): VisitorRet

  def visitInstructionList(instructionList: InstructionList, state: ProgramState): VisitorRet

  def visitTerminalBlock(basicBlock: BasicBlock, state: ProgramState): VisitorRet

  def visitConditionalBlock(basicBlock: BasicBlock, state: ProgramState): VisitorRet

  def visitUnconditionalBlock(basicBlock: BasicBlock, state: ProgramState): VisitorRet

  def visitBasicBlocks(basicBlockList: BasicBlocksList, state: ProgramState): VisitorRet

}
