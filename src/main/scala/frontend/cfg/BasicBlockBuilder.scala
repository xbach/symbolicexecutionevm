package frontend.cfg

import com.typesafe.scalalogging.Logger
import frontend.ast.Opcode._
import frontend.ast._

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

object BasicBlockBuilder {
  val logger = Logger("BasicBlockBuilder")

  def buildBasicBlocks(program: InstructionList): Program = {
    logger.debug("Start building basic blocks")
    // Start address -> corresponding block
    val basicBlocks = new mutable.HashMap[BigInt, BasicBlock]()
    val staticEdges =  new mutable.HashMap[BigInt, BigInt]()

    val jumpsDestinations: ArrayBuffer[BigInt] = new ArrayBuffer[BigInt]()

    def addJumpDestination(dest: BigInt) = jumpsDestinations += dest

    var justAfterJumpi = false
    var lastJumpi: Instruction = null

    def addStaticEdge(instr: Instruction, jumpi: Instruction) = {
      if(justAfterJumpi)
        staticEdges += jumpi.getAddr() -> instr.getAddr()
      justAfterJumpi = false
    }

    program.insts.foldLeft(ArrayBuffer[Instruction]())((res, instruction) => {
      instruction.getOpCode() match {
        case JUMPDEST => {
          addJumpDestination(instruction.getAddr())
          if(res.isEmpty) {
            res.append(instruction)
            addStaticEdge(instruction, lastJumpi)
            res
          } else {
            val instructionList = InstructionList(res)
            val unconditionalBlock = UnconditionalBlock(instructionList, res(0).getAddr())
            basicBlocks.put(unconditionalBlock.startAdrr, unconditionalBlock)
            // This is for the case of JUMPDEST blah blah (not jump, jumpi, or terminal nodes) JUMPDEST
            val newBlk = new ArrayBuffer[Instruction]()
            newBlk.append(instruction)
            staticEdges += res(0).getAddr() -> instruction.getAddr()
            newBlk
          }
        }
        case JUMPI => {
          res.append(instruction)
          val instructionList = InstructionList(res)
          val conditionalBlock = ConditionalBlock(instructionList, res(0).getAddr())
          // Each basic block has start address as the first instruction in its list
          basicBlocks.put(conditionalBlock.startAdrr, conditionalBlock)
          justAfterJumpi = true
          lastJumpi = instruction
          new ArrayBuffer[Instruction]()
        }
        case JUMP => {
          res.append(instruction)
          val instructionList = InstructionList(res)
          val unconditionalBlock = UnconditionalBlock(instructionList, res(0).getAddr())
          // Each basic block has start address as the first instruction in its list
          basicBlocks.put(unconditionalBlock.startAdrr, unconditionalBlock)
          addStaticEdge(instruction, lastJumpi)
          new ArrayBuffer[Instruction]()
        }

        case STOP | INVALID | REVERT| RETURN | SUICIDE => {
          res.append(instruction)
          val instructionList = InstructionList(res)
          val terminalBlock = TerminalBlock(instructionList, res(0).getAddr())
          // Each basic block has start address as the first instruction in its list
          basicBlocks.put(terminalBlock.startAdrr, terminalBlock)
          addStaticEdge(instruction, lastJumpi)
          new ArrayBuffer[Instruction]()
        }
        case _ => {
          res.append(instruction)
          addStaticEdge(instruction, lastJumpi)
          res
        }
      }
    })
    // Sanity check
    var failed = false
    jumpsDestinations.foreach(j => {
      if(!basicBlocks.contains(j)) {
        failed = true
        var x = false
        basicBlocks.foreach { case (_, blk) => {
          if (blk.getInstructionList().insts.filter(p => p.getAddr() == j).size > 0) {
            println("----: " + blk)
            x = true
          }
        }}
        if(!x)
          println("=== Not found any block start with jump addr: "+j)

      }
    })
    assert(!failed)
    BasicBlocksList(basicBlocks, jumpsDestinations, staticEdges)
  }
}
