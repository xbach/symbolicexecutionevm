package utils

import java.math.BigInteger

import backend.symexec.{FormulaItem, IntItem, StackItem}
import com.microsoft.z3._
import com.typesafe.scalalogging.Logger
import frontend.parser.Source
import play.api.libs.json.{JsNumber, JsObject}

import scala.collection.mutable.ArrayBuffer

object Utils {

  val logger = Logger("Utils")

  val delim = if(isWindows) "_" else ":"

  def checkSat(solver: Solver, popIfException: Boolean = true) = {
    try{
      val ret = solver.check()
      if(ret == Status.UNKNOWN)
        throw new Z3Exception(solver.getReasonUnknown)
      ret
    } catch {
      case e: Throwable => {
        if(popIfException)
          solver.pop()
        throw e
      }
    }
  }

  def ceil32(x: BigInt) ={
    if (x % 32 == 0)
      x
    else x + 32 - (x % 32)
  }

  def runCmd(cmd: String): String = {
    import sys.process._
    val stdout = new StringBuilder
    logger.debug("Running command: "+cmd)
    cmd ! ProcessLogger(
      stdout append _+"\n",
      _ => ())
    stdout.toString()
  }

  def fileWriter(file: String, content: String) ={
    import java.io.BufferedWriter
    import java.io.FileWriter
    import java.io.IOException
    var bw: BufferedWriter = null
    var fw: FileWriter = null

    try {
      fw = new FileWriter(file)
      bw = new BufferedWriter(fw)
      bw.write(content)
    } catch {
      case e: IOException =>
        e.printStackTrace()
    } finally try {
      if (bw != null) bw.close()
      if (fw != null) fw.close()
    } catch {
      case ex: IOException =>
        ex.printStackTrace()
    }
  }

  def getOsName: String = {
    System.getProperty("os.name")
  }

  def isWindows: Boolean = getOsName.startsWith("Windows")

  def toUnsigned(number: BigInt): BigInt = {
    val x = BigInt(BigInteger.ONE.shiftLeft(256))
    val n = number
    if(n.bigInteger.signum() < 0) {
      n + x
    }
    else n
  }

  def convertOffset2LineColumn(pos: JsObject, source: Source): ((Int, Int), (Int, Int)) = {
    val begin = pos("begin").asInstanceOf[JsNumber].value.toInt
    val end = pos("end").asInstanceOf[JsNumber].value.toInt
    if (begin >= 0 && (end - begin + 1) >= 0){
      val retBegin = convertFromCharPos(begin, source)
      val retEnd = convertFromCharPos(end, source)
      return (retBegin, retEnd)
    }

    return (null, null)
  }

  private def convertFromCharPos(pos: Int, source: Source): (Int, Int) = {
    var line = findLowerBound(pos, source.lineBreakPositions)
    if (source.lineBreakPositions(line) != pos)
      line = line + 1
    var begin_col = 0
    if (line != 0) begin_col = source.lineBreakPositions(line - 1) + 1
    val col = pos - begin_col
    return (line, col)
  }

  private def findLowerBound(target: Int, array: ArrayBuffer[Int]): Int = {
    var start = 0
    var length = array.size
    while (length > 0){
      val half = length >> 1
      val middle = start + half
      if (array(middle) <= target) {
        length = length - 1 - half
        start = middle + 1
      } else
        length = half
    }
    return start - 1
  }

}
