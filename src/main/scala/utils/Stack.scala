package utils


class Stack[E] {
  private var s = List[E]()
  def pop(): E = {
    if(s.size == 0)
      throw new RuntimeException("Cannot Pop Stack of size 0")

    val head = s.head
    s = s.tail
    head
  }

  def push(e: E): Unit = {
    s = e :: s
    //logger.info("Pushed: "+s)
  }

  def update(i: Int, e: E) = s = s.updated(i, e)

  def peek(): E = s.head

  def atPos(i: Int) = s(i)

  def isEmpty(): Boolean = s.isEmpty

  def iter(fun: E => Unit) = s.foreach(fun)

  def size(): Int = s.length

  def copy(st: Stack[E]) = {
    val cpst = s.foldLeft(List[E]()){(res, i) => res :+ i}
    st.setStack(cpst)
  }

  def print(): Unit = s.foreach(e => println(e))

  def getStack(): List[E] = s

  def setStack(st: List[E]) = s = st
}
object TestStack {
  def main(args: Array[String]): Unit = {
    val s = new Stack[Int]
    //s.pop()
    s.push(10)
    s.push(20)
    s.push(30)
    s.print()
    println("=====")
    //s.pop()
    //s.print()

    val cp = new Stack[Int]
    cp.print()
    s.copy(cp)
    cp.print()
    println("=====")
    s.pop()
    s.print()
    println("=====")
    cp.print()

  }
}
