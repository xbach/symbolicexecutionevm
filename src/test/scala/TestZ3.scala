import java.math.BigInteger
import java.util

import backend.symexec.IntItem
import com.microsoft.z3.{ArithExpr, BitVecExpr, Context}
import utils.Utils

object TestZ3 {
  def main(args: Array[String]): Unit = {
    val cfg = new util.HashMap[String, String]()
    val z3 = new Context(cfg)
    val UNSIGNED_BOUND_NUMBER = z3.mkBV((Math.pow(2,256) - 1).toInt, 256)

    val rep = "     x y".replaceAll("\\s","")
    println(rep)

    val i = z3.mkInt(0)
    // 115792089237316195423570985008687907853269984665640564039457584007913129639935
    val bv = z3.mkBVAND(z3.mkBVNot(z3.mkInt2BV(256, i)), UNSIGNED_BOUND_NUMBER)
    println(bv.simplify())
    val notZero = z3.mkBVNot(z3.mkInt2BV(256, i))
    val zero = z3.mkBV(0, 256)
    //val zz = z3.mkGt(zero.asInstanceOf[ArithExpr], zero.asInstanceOf[ArithExpr])
    println("Zero: "+zero +" "+zero.isBV +" "+i.isBVNumeral +" "+i.isNumeral)
    println(notZero.simplify())
    println(notZero.simplify().isInstanceOf[BitVecExpr])
    println(z3.mkBV2Int(notZero, true).simplify())

    val v = z3.mkBVConst("kkk", 256)

    val x = z3.mkITE(z3.mkEq(zero, v), z3.mkBV(1, 256), z3.mkBV(0, 256))
    println("If: "+x)
    println(x.simplify())

    val k = Utils.toUnsigned(-4)
    println(k)
    val k1 = Utils.toUnsigned(5)
    println(k1)

    val sbi = "0x0100000000000000000000000000000000000000000000000000000000"
    val bi = new BigInteger(sbi.replace("0x",""), 16)
    println(bi)
    println(new BigInt(bi).toLong)
    // 115792089237316195423570985008687907853269984665640564039457584007913129639932

    //unsigned
    println("Testing unsigned ...")
    val first = IntItem(BigInt("000000000000000000000000000000000000000000000000000000000f", 16))
    val second = IntItem(BigInt("0000000000000000000000000000000000000000000000000000000001", 16))
    println(first.toUnsigned())
    println(second.toUnsigned())
    val div = z3.mkDiv(z3.mkInt(first.toUnsigned().toString()), z3.mkInt(first.toUnsigned().toString()))
    println(div.simplify())
    println(first.toUnsigned()/second.toUnsigned())

  }
}
